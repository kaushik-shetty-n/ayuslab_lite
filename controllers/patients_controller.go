package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	//"github.com/astaxie/beego/validation"
	models "ayuslab_lite/models"
	//"strconv"
)

type PatientsController struct {
	beego.Controller
}

func (patient *PatientsController) Index() {
	patient.Layout = "layouts/basic-layout.tpl"
	patient.LayoutSections = make(map[string]string)
	patient.LayoutSections["Header"] = "header.tpl"
	patient.LayoutSections["Footer"] = "footer.tpl"
	patient.TplName = "patients/index.tpl"

	flash := beego.ReadFromRequest(&patient.Controller)

	if ok := flash.Data["error"]; ok != "" {
		// Display error messages
		patient.Data["errors"] = ok
	}

	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		patient.Data["notices"] = ok
	}

	o := orm.NewOrm()
	o.Using("default")

	var patients []*models.Patient
	num, err := o.QueryTable("patients").All(&patients)
	fmt.Printf("Returned Rows Num: %s, %s", num, err)

	if err != orm.ErrNoRows && num > 0 {
		patient.Data["records"] = patients
	}
}