package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	//"github.com/astaxie/beego/validation"
	models "ayuslab_lite/models"
	//"strconv"
)

type BillsController struct {
	beego.Controller
}

func (bill *BillsController) Index() {
	bill.Layout = "layouts/basic-layout.tpl"
	bill.LayoutSections = make(map[string]string)
	bill.LayoutSections["Header"] = "header.tpl"
	bill.LayoutSections["Footer"] = "footer.tpl"
	bill.TplName = "bills/index.tpl"

	flash := beego.ReadFromRequest(&bill.Controller)

	if ok := flash.Data["error"]; ok != "" {
		// Display error messages
		bill.Data["errors"] = ok
	}

	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		bill.Data["notices"] = ok
	}

	o := orm.NewOrm()
	o.Using("default")

	var bills []*models.Bill
	num, err := o.QueryTable("bills").All(&bills)
	fmt.Printf("Returned Rows Num: %s, %s", num, err)

	if err != orm.ErrNoRows && num > 0 {
		bill.Data["records"] = bills
	}
}