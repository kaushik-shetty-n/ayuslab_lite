package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	//"github.com/astaxie/beego/validation"
	models "ayuslab_lite/models"
	//"strconv"
)

type PrescriptionsController struct {
	beego.Controller
}

func (prescription *PrescriptionsController) Index() {
	prescription.Layout = "layouts/basic-layout.tpl"
	prescription.LayoutSections = make(map[string]string)
	prescription.LayoutSections["Header"] = "header.tpl"
	prescription.LayoutSections["Footer"] = "footer.tpl"
	prescription.TplName = "prescriptions/index.tpl"

	flash := beego.ReadFromRequest(&prescription.Controller)

	if ok := flash.Data["error"]; ok != "" {
		// Display error messages
		prescription.Data["errors"] = ok
	}

	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		prescription.Data["notices"] = ok
	}

	o := orm.NewOrm()
	o.Using("default")

	var prescriptions []*models.Prescription
	num, err := o.QueryTable("prescriptions").RelatedSel().All(&prescriptions)
	fmt.Printf("Returned Rows Num: %s, %s", num, err)

	if err != orm.ErrNoRows && num > 0 {
		prescription.Data["records"] = prescriptions
	}
}