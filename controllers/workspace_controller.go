package controllers

import (
	//"fmt"
	"github.com/astaxie/beego"
	//"github.com/astaxie/beego/orm"
	//"github.com/astaxie/beego/validation"
	//models "ayuslab_lite/models"
	//"strconv"
)

type WorkspaceController struct {
	beego.Controller
}

func (workspace *WorkspaceController) Dashboard() {
	workspace.Layout = "layouts/basic-layout.tpl"
	workspace.LayoutSections = make(map[string]string)
	workspace.LayoutSections["Header"] = "header.tpl"
	workspace.LayoutSections["Footer"] = "footer.tpl"
	workspace.TplName = "workspace/dashboard.tpl"
}