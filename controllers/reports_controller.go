package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	//"github.com/astaxie/beego/validation"
	models "ayuslab_lite/models"
	//"strconv"
)

type ReportsController struct {
	beego.Controller
}

func (report *ReportsController) Index() {
	report.Layout = "layouts/basic-layout.tpl"
	report.LayoutSections = make(map[string]string)
	report.LayoutSections["Header"] = "header.tpl"
	report.LayoutSections["Footer"] = "footer.tpl"
	report.TplName = "reports/index.tpl"

	flash := beego.ReadFromRequest(&report.Controller)

	if ok := flash.Data["error"]; ok != "" {
		// Display error messages
		report.Data["errors"] = ok
	}

	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		report.Data["notices"] = ok
	}

	o := orm.NewOrm()
	o.Using("default")

	var reports []*models.ReportDispatches
	num, err := o.QueryTable("report_dispatches").All(&reports)
	fmt.Printf("Returned Rows Num: %s, %s", num, err)

	if err != orm.ErrNoRows && num > 0 {
		report.Data["records"] = reports
	}
}