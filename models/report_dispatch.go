package models

import (
	//"errors"
	//"fmt"
	//"reflect"
	//"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type ReportDispatches struct {
	Id                           int       `orm:"column(id);auto"`
	VenueId                      int       `orm:"column(venue_id);null"`
	Title                        string    `orm:"column(title);size(255);null"`
	Comment                      string    `orm:"column(comment);size(255);null"`
	NextVisitDate                time.Time `orm:"column(next_visit_date);type(date);null"`
	NextVisitNote                string    `orm:"column(next_visit_note);size(255);null"`
	SampleReceivedDate           time.Time `orm:"column(sample_received_date);type(datetime);null"`
	ReportDoneBy                 int       `orm:"column(report_done_by);null"`
	CreatedAt                    time.Time `orm:"column(created_at);type(datetime);null"`
	UpdatedAt                    time.Time `orm:"column(updated_at);type(datetime);null"`
	LeftSignature                int       `orm:"column(left_signature);null"`
	MiddleSignature              int       `orm:"column(middle_signature);null"`
	RightSignature               int       `orm:"column(right_signature);null"`
	NoOfTimePrint                int       `orm:"column(no_of_time_print);null"`
	PrintBill                    int8      `orm:"column(print_bill);null"`
	PrintCenter                  int8      `orm:"column(print_center);null"`
	ReportingTime                time.Time `orm:"column(reporting_time);type(datetime);null"`
	PatientId                    int       `orm:"column(patient_id);null"`
	PrescriptionId               int       `orm:"column(prescription_id);null"`
	Deleted                      int8      `orm:"column(deleted);null"`
	SendSmsPatient               int8      `orm:"column(send_sms_patient);null"`
	PrintSignatureOnEveryPage    int8      `orm:"column(print_signature_on_every_page);null"`
	InsuranceDisplayTextInReport string    `orm:"column(insurance_display_text_in_report);null"`
	EmailedToPatientOn           time.Time `orm:"column(emailed_to_patient_on);type(datetime);null"`
	SmsReport                    int8      `orm:"column(sms_report);null"`
	SendEmail                    int8      `orm:"column(send_email);null"`
	Uuid                         int64     `orm:"column(uuid);null"`
}

func (t *ReportDispatches) TableName() string {
	return "report_dispatches"
}

func init() {
	orm.RegisterModel(new(ReportDispatches))
}
