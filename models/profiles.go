package models

import (
	//"errors"
	//"fmt"
	//"reflect"
	//"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type Profiles struct {
	Id                   int       `orm:"column(id);auto"`
	UserId               int       `orm:"column(user_id);null"`
	Address              string    `orm:"column(address);null"`
	LandLine             string    `orm:"column(land_line);size(255);null"`
	Mobile               string    `orm:"column(mobile);size(255);null"`
	OtherInformation     string    `orm:"column(other_information);null"`
	CreatedAt            time.Time `orm:"column(created_at);type(datetime);null"`
	UpdatedAt            time.Time `orm:"column(updated_at);type(datetime);null"`
	Uuid                 int64     `orm:"column(uuid);null"`
	Qualification        string    `orm:"column(qualification);size(255);null"`
	Designation          string    `orm:"column(designation);size(255);null"`
	SignatureFileName    string    `orm:"column(signature_file_name);size(255);null"`
	SignatureContentType string    `orm:"column(signature_content_type);size(255);null"`
	SignatureFileSize    int       `orm:"column(signature_file_size);null"`
	SignatureUpdatedAt   time.Time `orm:"column(signature_updated_at);type(datetime);null"`
}

func (t *Profiles) TableName() string {
	return "profiles"
}

func init() {
	orm.RegisterModel(new(Profiles))
}