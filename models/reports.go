package models

import (
	//"errors"
	//"fmt"
	//"reflect"
	//"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type Report struct {
	Id                  int       `orm:"column(id);auto"`
	ReportDispatchId    int       `orm:"column(report_dispatch_id);null"`
	//PrescribedTestId    int       `orm:"column(prescribed_test_id);null"`
	PrescribedTest      *PrescribedTest `orm:"rel(fk)"`
	PatientId           int       `orm:"column(patient_id);null"`
	Position            int       `orm:"column(position);null"`
	DobOrApproximateDob time.Time `orm:"column(dob_or_approximate_dob);type(date);null"`
	CreatedAt           time.Time `orm:"column(created_at);type(datetime);null"`
	UpdatedAt           time.Time `orm:"column(updated_at);type(datetime);null"`
	OutsourceLabId      int       `orm:"column(outsource_lab_id);null"`
	OutsourceLabAmount  float64   `orm:"column(outsource_lab_amount);null;digits(10);decimals(2)"`
	Active              int8      `orm:"column(active);null"`
	Draft               int8      `orm:"column(draft);null"`
	Uuid                int64     `orm:"column(uuid);null"`
	CreatorId           int       `orm:"column(creator_id);null"`
	ApprovalRequired    int8      `orm:"column(approval_required);null"`
	Approved            int8      `orm:"column(approved);null"`
	ApproverId          int       `orm:"column(approver_id);null"`
}

func (t *Report) TableName() string {
	return "reports"
}

func init() {
	orm.RegisterModel(new(Report))
}
