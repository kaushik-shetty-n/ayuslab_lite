package models

import (
	//"errors"
	//"fmt"
	//"reflect"
	//"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type PrescribedTest struct {
	Id                 int       `orm:"column(id);auto"`
	//PrescriptionId     int       `orm:"column(prescription_id);null"`
	Prescription       *Prescription `orm:"rel(fk)"`
	LabTest            *LabTest      `orm:"rel(fk)"`
	//LabTestId          int       `orm:"column(lab_test_id);null"`
	CreatedAt          time.Time `orm:"column(created_at);type(datetime);null"`
	UpdatedAt          time.Time `orm:"column(updated_at);type(datetime);null"`
	Uuid               int64     `orm:"column(uuid);null"`
	OutsourceLabId     int       `orm:"column(outsource_lab_id);null"`
	OutsourceLabAmount float64   `orm:"column(outsource_lab_amount);null;digits(10);decimals(2)"`
  Reports            []* Report `orm:"reverse(many)"`
}

func (t *PrescribedTest) TableName() string {
	return "prescribed_tests"
}

func init() {
	orm.RegisterModel(new(PrescribedTest))
}

func (pres PrescribedTest) Report_state() (str string) {
  o := orm.NewOrm()
  exist := o.QueryTable("reports").Filter("prescribed_test_id", pres.Id).Exist()
  if exist {
    str = "has_report"
  }else{
    str = "has_no_report"
  }
  return str
}