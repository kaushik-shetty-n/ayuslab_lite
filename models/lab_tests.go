package models

import (
	//"errors"
	//"fmt"
	//"reflect"
	//"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type LabTest struct {
	Id                 int       `orm:"column(id);auto"`
	LabId              int       `orm:"column(lab_id);null"`
	Name               string    `orm:"column(name);size(255);null"`
	LabCategoryId      int       `orm:"column(lab_category_id);null"`
	Charges            float64   `orm:"column(charges);null;digits(10);decimals(2)"`
	PrintLabTestName   int8      `orm:"column(print_lab_test_name);null"`
	Instructions       string    `orm:"column(instructions);null"`
	TableFormat        int8      `orm:"column(table_format);null"`
	Active             int8      `orm:"column(active);null"`
	CreatedAt          time.Time `orm:"column(created_at);type(datetime);null"`
	UpdatedAt          time.Time `orm:"column(updated_at);type(datetime);null"`
	MasterLabTestId    int       `orm:"column(master_lab_test_id);null"`
	Position           int       `orm:"column(position);null"`
	VenueId            int       `orm:"column(venue_id);null"`
	OutsourceLabId     int       `orm:"column(outsource_lab_id);null"`
	OutsourceLabAmount float64   `orm:"column(outsource_lab_amount);null;digits(10);decimals(2)"`
	SpecimenMasterId   int       `orm:"column(specimen_master_id);null"`
	ShortCode          string    `orm:"column(short_code);size(255);null"`
	CalculationFormula string    `orm:"column(calculation_formula);null"`
	SpecialTest        int8      `orm:"column(special_test);null"`
	Uuid               int64     `orm:"column(uuid);null"`
	Prescriptions      []*Prescription    `orm:"rel(m2m);rel_through(ayuslab_lite/models.PrescribedTest)"`
}

func (t *LabTest) TableName() string {
	return "lab_tests"
}

func init() {
	orm.RegisterModel(new(LabTest))
}
