package models

import (
	//"errors"
	//"fmt"
	//"reflect"
	//"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type Venue struct {
	Id                           int       `orm:"column(id);auto"`
	Name                         string    `orm:"column(name);size(255);null"`
	Address                      string    `orm:"column(address);size(255);null"`
	LabId                        int       `orm:"column(lab_id);null"`
	CreatedAt                    time.Time `orm:"column(created_at);type(datetime);null"`
	UpdatedAt                    time.Time `orm:"column(updated_at);type(datetime);null"`
	Email                        string    `orm:"column(email);size(255);null"`
	LandLine                     string    `orm:"column(land_line);size(255);null"`
	Phone                        string    `orm:"column(phone);size(255);null"`
	ContactPerson                string    `orm:"column(contact_person);size(255);null"`
	ReportBottomLabel            string    `orm:"column(report_bottom_label);size(255);null"`
	ReportBottomName             string    `orm:"column(report_bottom_name);size(255);null"`
	SignatureAtFooter            int8      `orm:"column(signature_at_footer);null"`
	PrintUnitInNormalRange       int8      `orm:"column(print_unit_in_normal_range);null"`
	PrintTestDoneBy              int8      `orm:"column(print_test_done_by);null"`
	PrintBill                    int8      `orm:"column(print_bill);null"`
	PrintCenter                  int8      `orm:"column(print_center);null"`
	PrintContact                 int8      `orm:"column(print_contact);null"`
	NormalRangeSetting           string    `orm:"column(normal_range_setting);size(255);null"`
	LetterheadFileName           string    `orm:"column(letterhead_file_name);size(255);null"`
	LetterheadFileSize           int       `orm:"column(letterhead_file_size);null"`
	LetterheadFileContentType    string    `orm:"column(letterhead_file_content_type);size(255);null"`
	PrintSignatureOnEveryPage    int8      `orm:"column(print_signature_on_every_page);null"`
	LabTestNoAutoincrement       int8      `orm:"column(lab_test_no_autoincrement);null"`
	DrawBoxInReport              int8      `orm:"column(draw_box_in_report);null"`
	SmsReport                    int8      `orm:"column(sms_report);null"`
	FooterImageFileName          string    `orm:"column(footer_image_file_name);size(255);null"`
	FooterImageFileContentType   string    `orm:"column(footer_image_file_content_type);size(255);null"`
	FooterImageFileSize          string    `orm:"column(footer_image_file_size);size(255);null"`
	Prefix                       string    `orm:"column(prefix);size(255);null"`
	Suffix                       string    `orm:"column(suffix);size(255);null"`
	Uuid                         int64     `orm:"column(uuid);null"`
	SyncEnabled                  int8      `orm:"column(sync_enabled);null"`
	AllowOutsourceForStaff       int8      `orm:"column(allow_outsource_for_staff);null"`
	ResultApprovalRequired       int8      `orm:"column(result_approval_required);null"`
	AutoDayClose                 int8      `orm:"column(auto_day_close);null"`
	AddSignatureInReportDispatch int8      `orm:"column(add_signature_in_report_dispatch);null"`
	BillNoPrefix                 string    `orm:"column(bill_no_prefix);size(255);null"`
	Prescriptions             []*Prescription `orm:"reverse(many)"`
}

func (t *Venue) TableName() string {
	return "venues"
}

func init() {
	orm.RegisterModel(new(Venue))
}
