package models

import (
	//"errors"
	//"fmt"
	//"reflect"
	//"strings"
  "html/template"
	"time"

	"github.com/astaxie/beego/orm"
)

type Doctor struct {
	Id               int       `orm:"column(id);auto"`
	LabId            int       `orm:"column(lab_id);null"`
	Initial          string    `orm:"column(initial);size(255);null"`
	Name             string    `orm:"column(name);size(255);null"`
	Address          string    `orm:"column(address);null"`
	LandLine         string    `orm:"column(land_line);size(255);null"`
	Mobile           string    `orm:"column(mobile);size(255);null"`
	Email            string    `orm:"column(email);size(255);null"`
	Colorcode        string    `orm:"column(colorcode);size(255);null"`
	Speciality       string    `orm:"column(speciality);size(255);null"`
	Qualification    string    `orm:"column(qualification);size(255);null"`
	OtherInformation string    `orm:"column(other_information);null"`
	SendSms          int8      `orm:"column(send_sms);null"`
	SendEmail        int8      `orm:"column(send_email);null"`
	CreatedAt        time.Time `orm:"column(created_at);type(datetime);null"`
	UpdatedAt        time.Time `orm:"column(updated_at);type(datetime);null"`
	VenueId          int       `orm:"column(venue_id);null"`
	Active           int8      `orm:"column(active);null"`
	ClientPk         string    `orm:"column(client_pk);size(255);null"`
	AllowCommission  int8      `orm:"column(allow_commission);null"`
	Uuid             int64     `orm:"column(uuid);null"`
	Prescriptions             []*Prescription `orm:"reverse(many)"`

}

func (t *Doctor) TableName() string {
	return "doctors"
}

func init() {
	orm.RegisterModel(new(Doctor))
}

func (doc Doctor) Name_with_initial() string {
  s := ""
  s = s + doc.Initial + " "
  s = s + doc.Name
  return s
}

func (doc Doctor) Name_with_degree() (s template.HTML) {
  str := doc.Name_with_initial()
  if err := doc.Qualification; err != ""{
    str = str + `, <span class="qualification"> `+ doc.Qualification +`</span>`
  }
  s = template.HTML(str)
  return s
}  

