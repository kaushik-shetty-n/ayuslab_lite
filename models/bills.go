package models

import (
	//"errors"
	//"fmt"
	//"reflect"
	//"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type Bill struct {
	Id              int       `orm:"column(id);auto"`
	BillNo          string    `orm:"column(bill_no);size(255);null"`
	PatientId       int       `orm:"column(patient_id);null"`
	DoctorId        int       `orm:"column(doctor_id);null"`
	VenueId         int       `orm:"column(venue_id);null"`
	PrescriptionId  int       `orm:"column(prescription_id);null"`
	BilledTime      time.Time `orm:"column(billed_time);type(datetime);null"`
	Discount        float64   `orm:"column(discount);null;digits(10);decimals(2)"`
	Status          string    `orm:"column(status);size(255);null"`
	PaymentStatus   string    `orm:"column(payment_status);size(255);null"`
	CreatedAt       time.Time `orm:"column(created_at);type(datetime);null"`
	UpdatedAt       time.Time `orm:"column(updated_at);type(datetime);null"`
	LabId           int       `orm:"column(lab_id);null"`
	WriteOffAmount  float64   `orm:"column(write_off_amount);null;digits(10);decimals(2)"`
	BillingNameId   int       `orm:"column(billing_name_id);null"`
	InsuranceNo     string    `orm:"column(insurance_no);size(255);null"`
	TotalAmount     float64   `orm:"column(total_amount);null;digits(10);decimals(2)"`
	BillType        string    `orm:"column(bill_type);size(255);null"`
	PaymentType     string    `orm:"column(payment_type);size(255);null"`
	ReferredById    int       `orm:"column(referred_by_id);null"`
	Deleted         int8      `orm:"column(deleted);null"`
	ClientPk        string    `orm:"column(client_pk);size(255);null"`
	CreatorId       int       `orm:"column(creator_id);null"`
	PercentDiscount float64   `orm:"column(percent_discount);null;digits(10);decimals(0)"`
	Remark          string    `orm:"column(remark);null"`
	Uuid            int64     `orm:"column(uuid);null"`
}

func (t *Bill) TableName() string {
	return "bills"
}

func init() {
	orm.RegisterModel(new(Bill))
}