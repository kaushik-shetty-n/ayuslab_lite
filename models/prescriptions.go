package models

import (
	//"errors"
	"fmt"
	//"reflect"
	//"strings"
  //"html/template"
	"time"

	"github.com/astaxie/beego/orm"

)

type Prescription struct {
	Id                  int       `orm:"column(id);auto"`
	Date                time.Time `orm:"column(date);type(date);null"`
	DobOrApproximateDob time.Time `orm:"column(dob_or_approximate_dob);type(date);null"`
	//PatientId           int       `orm:"column(patient_id);null"`
	Patient             *Patient  `orm:"rel(fk)"`
	//DoctorId            int       `orm:"column(doctor_id);null"`
  Doctor              *Doctor    `orm:"rel(fk)"`
	//VenueId             int       `orm:"column(venue_id);null"`
	Venue               *Venue    `orm:"rel(fk)"`
	AppointmentId       int       `orm:"column(appointment_id);null"`
	//ReferredById        int       `orm:"column(referred_by_id);null"`
	ReferredBy          *PrescriptionReference `orm:"rel(fk)"`
	CreatorId           int       `orm:"column(creator_id);null"`
	Note                string    `orm:"column(note);size(255);null"`
	Status              string    `orm:"column(status);size(255);null"`
	CreatedAt           time.Time `orm:"column(created_at);type(datetime);null"`
	UpdatedAt           time.Time `orm:"column(updated_at);type(datetime);null"`
	PrescriptionType    string    `orm:"column(prescription_type);size(255);null"`
	PrintReferredBy     int8      `orm:"column(print_referred_by);null"`
	Cancelled           int8      `orm:"column(cancelled);null"`
	LabNo               string    `orm:"column(lab_no);size(255);null"`
	Deleted             int8      `orm:"column(deleted);null"`
	ClientPk            string    `orm:"column(client_pk);size(255);null"`
	InsuranceCompanyId  int       `orm:"column(insurance_company_id);null"`
	IpNo                string    `orm:"column(ip_no);size(255);null"`
	OpNo                string    `orm:"column(op_no);size(255);null"`
	Uuid                int64     `orm:"column(uuid);null"`
	LabTests     []*LabTest `orm:"reverse(many)"`
}

func (t *Prescription) TableName() string {
	return "prescriptions"
}

func init() {
	orm.RegisterModel(new(Prescription))
}

func (pres Prescription) Ip_op_with_detail() (string){
	if err := pres.PrescriptionType; err == ""{
		return ""
	}
  if err := pres.Op_patient_prescription(); err{
  	return "OP/" + pres.OpNo
  }
  if err := pres.In_patient_prescription(); err{
  	return "IP/" + pres.IpNo
  }
  return ""
}

func (pres Prescription) Op_patient_prescription() (b bool){
  if pres.PrescriptionType == "OP" {
  	b = true
  }
  return b
}

func (pres Prescription) In_patient_prescription() (b bool){
  if pres.PrescriptionType == "IP"{
  	b = true
  }
  return b
}

func (pres Prescription) Test_code() string{
  str := ""
  var maps []orm.Params
  o := orm.NewOrm()
  num, err := o.QueryTable("LabTests").Values(&maps)
  if err == nil {
    fmt.Printf("Result Nums: %d\n", num)
    for _, m := range maps {
        fmt.Println(m["Id"], m["Name"])
    }
  }
  return str
}

func (pres Prescription) Extra_lab_test_details() string{
  str := ""
  var prescribed_tests [] *PrescribedTest 
  o := orm.NewOrm()
  num, err := o.QueryTable("prescribed_tests").Filter("prescription_id", pres.Id).All(&prescribed_tests) 
  if err == nil {
  	fmt.Printf("Result Nums: %d\n", num)
  	for _, row := range prescribed_tests {
  		str = str + `<div class='`+ row.Report_state()+ `'>` 
  		str = str + row.LabTest.Name
  		str = str + `</div>`
  	}
  } 	
  //s = template.HTML(str)
  return str
}
