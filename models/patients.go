package models

import (
	//"errors"
	//"fmt"
	//"reflect"
	//"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type Patient struct {
	Id                   int       `orm:"column(id);auto"`
	VenueId              int       `orm:"column(venue_id);null"`
	PatientNo            string    `orm:"column(patient_no);size(255);null"`
	Initial              string    `orm:"column(initial);size(255);null"`
	Name                 string    `orm:"column(name);size(255);null"`
	Dob                  time.Time `orm:"column(dob);type(date);null"`
	ApproximateDob       time.Time `orm:"column(approximate_dob);type(date);null"`
	Gender               string    `orm:"column(gender);size(255);null"`
	MaritalStatus        string    `orm:"column(marital_status);size(255);null"`
	Address              string    `orm:"column(address);null"`
	LandLine             string    `orm:"column(land_line);size(255);null"`
	Mobile               string    `orm:"column(mobile);size(255);null"`
	Email                string    `orm:"column(email);size(255);null"`
	Guardian             string    `orm:"column(guardian);size(255);null"`
	GuardianRelationship string    `orm:"column(guardian_relationship);size(255);null"`
	SendSms              int8      `orm:"column(send_sms);null"`
	SendEmail            int8      `orm:"column(send_email);null"`
	CreatedAt            time.Time `orm:"column(created_at);type(datetime);null"`
	UpdatedAt            time.Time `orm:"column(updated_at);type(datetime);null"`
	LabId                int       `orm:"column(lab_id);null"`
	Deleted              int8      `orm:"column(deleted);null"`
	ClientPk             string    `orm:"column(client_pk);size(255);null"`
	Uuid                 int64     `orm:"column(uuid);null"`
  Prescriptions       []*Prescription `orm:"reverse(many)"`
}

func (t *Patient) TableName() string {
	return "patients"
}

func init() {
	orm.RegisterModel(new(Patient))
}

func (p Patient) Patient_name_with_initial() string {
  name_with_initial := p.Initial + " " + p.Name		
	//fmt.Printf("name_with_initial: %v \n", p)
	return name_with_initial
}

func (p Patient) Check_for_age() string {
	//a := if self.dob.blank?
      //Date.diff(self.dob, Date.today)
    //else
      //Date.diff(approximate_dob, Date.today) rescue ''
    //end

    e := ""
    //unless a.blank?
      //if a[:year] <= 5
        //e += self.approximate_dob_and_age
      //else a[:year] > 5
        //e += "#{a[:year]} yrs"
      //end
    //end

  return e
}

