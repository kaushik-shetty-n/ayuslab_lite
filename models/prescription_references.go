package models

import (
	//"errors"
	//"fmt"
	//"reflect"
	//"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type PrescriptionReference struct {
	Id               int       `orm:"column(id);auto"`
	LabId            int       `orm:"column(lab_id);null"`
	Name             string    `orm:"column(name);size(255);null"`
	Address          string    `orm:"column(address);null"`
	ContactName      string    `orm:"column(contact_name);size(255);null"`
	ContactNumber    string    `orm:"column(contact_number);size(255);null"`
	CreatedAt        time.Time `orm:"column(created_at);type(datetime);null"`
	UpdatedAt        time.Time `orm:"column(updated_at);type(datetime);null"`
	VenueId          int       `orm:"column(venue_id);null"`
	Active           int8      `orm:"column(active);null"`
	Email            string    `orm:"column(email);size(255);null"`
	SendEmail        int8      `orm:"column(send_email);null"`
	AlternativeEmail string    `orm:"column(alternative_email);size(255);null"`
	Uuid             int64     `orm:"column(uuid);null"`
	Prescriptions       []*Prescription `orm:"reverse(many)"`

}

func (t *PrescriptionReference) TableName() string {
	return "prescription_references"
}

func init() {
	orm.RegisterModel(new(PrescriptionReference))
}