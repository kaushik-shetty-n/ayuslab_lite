<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset='utf-8'>
    <meta content='width=device-width, initial-scale=1.0' name='viewport'>
    <title>Ayuslab</title>
    <meta content="authenticity_token" name="csrf-param" />
    <meta content="pX4xV49KPgs6A+R65lhEjhxZVHObknaN82x1Nrn47aQ=" name="csrf-token" />
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="/static/css/bootstrap_and_overrides.css?body=1" media="all" rel="stylesheet" />
    <link href="/static/css/jquery-ui-1.10.3.custom.css?body=1" media="all" rel="stylesheet" />
    <link href="/static/css/bootstrap-wysihtml5.css?body=1" media="all" rel="stylesheet" />
    <link href="/static/css/jquery-ui-timepicker-addon.css?body=1" media="all" rel="stylesheet" />
    <link href="/static/css/select2.css?body=1" media="all" rel="stylesheet" />
    <link href="/static/css/bootstrap-modal.css?body=1" media="all" rel="stylesheet" />
    <link href="/static/css/comman.css?body=1" media="all" rel="stylesheet" />
    <link href="/static/css/summernote.css?body=1" media="all" rel="stylesheet" />
    <link href="/static/css/font-awesome.min.css?body=1" media="all" rel="stylesheet" />
    <link href="/static/css/application.css?body=1" media="all" rel="stylesheet" />
    <link href="/static/css/bootstrap-multiselect.css?body=1" media="all" rel="stylesheet" />
    <script src="/assets/twitter/bootstrap.js?body=1"></script>
    <script src="/static/js/jquery-ui-1.10.3.custom.js?body=1"></script>
    <script src="/static/js/jquery-ui-timepicker-addon.js?body=1"></script>
    <script src="/static/js/wysihtml5-0.3.0.js?body=1"></script>
    <script src="/static/js/bootstrap-wysihtml5.js?body=1"></script>
    <script src="/static/js/select2.js?body=1"></script>
    <script src="/static/js/prescription.js?body=1"></script>
    <script src="/static/js/bills.js?body=1"></script>
    <script src="/static/js/bootstrap-modalmanager.js?body=1"></script>
    <script src="/static/js/bootstrap-modal.js?body=1"></script>
    <script src="/static/js/bootstrap-multiselect.js?body=1"></script>
    <script src="/static/js/common.js?body=1"></script>
    <script src="/static/js/age_calculation_plugin.js?body=1"></script>
    <script src="/static/js/summernote.min.js?body=1"></script>
    <script src="/static/js/package.js?body=1"></script>
    <script src="/static/js/application.js?body=1"></script>
  </head>
  <body>
    <div class='navbar navbar-inverse navbar-fixed-top'>
      <div class='navbar-inner'>
        <div class='container-fluid'>
          <button class='btn btn-navbar' data-target='.nav-collapse' data-toggle='collapse' type='button'>
            <span class='icon-bar'></span>
            <span class='icon-bar'></span>
            <span class='icon-bar'></span>
          </button>
          <a class="brand" href="/workspace"><i class='fa fa-home fa-white'></i>  Ayuslab<span class='sub-title'>(Puttur)</span></a>
          <div class='nav-collapse collapse'>
            <ul class='nav'>
              <li class='activelink'>
                <a href="/workspace"><i class='fa fa-tasks'></i>  Workspace</a>
              </li>
              <li>
                <a href="/patients"><i class='fa fa-user'></i>  Patients</a>
              </li>
              <li>
                <a href="/prescriptions"><i class=' fa fa-list-alt fa-white'></i>  Prescriptions</a>
              </li>
              <li>
                <a href="/reports"><i class='fa fa-print fa-white'></i>  Reports</a>
              </li>
              <li>
                <a href="/bills"><i class=' fa fa-briefcase fa-white'></i>  Bills</a>
              </li>
              <li>
                <a href="/bill_summaries"><i class=' fa fa-signal fa-white'></i>  Statistics</a>
              </li>
              <li class='dropdown'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                  <i class='fa fa-wrench fa-white'></i>
                  Settings
                  <b class='caret'></b>
                </a>
                <ul class='dropdown-menu'>
                  <li class='black'><a href="/receipts">Add Receipts</a></li>
                  <li class='black'><a href="/bill_item_masters">Bill Item masters</a></li>
                  <li class='black'><a href="/billing_names">Billing names</a></li>
                  <li class='black'><a href="/expenses">Cash Payment voucher</a></li>
                  <li class='black'><a href="/cash_receipt_vouchers">Cash Receipt voucher</a></li>
                  <li class='black'><a href="/doctors">Doctors</a></li>
                  <li class='black'><a href="/test_classifications">Doctor&#39;s Incentive</a></li>
                  <li class='black'><a href="/signatures">E-Signatures</a></li>
                  <li class='black'><a href="/expense_lists">Expense masters</a></li>
                  <li class='black'><a href="/insurance_companies">Insurance Company Master</a></li>
                  <li class='black'><a href="/lab_test_price_lists">Lab Test Price lists</a></li>
                  <li class='black'><a href="/venue_members">Members</a></li>
                  <li class='black'><a href="/outsource_labs">Outsource labs</a></li>
                  <li class='black'><a href="/packages">Packages</a></li>
                  <li class='black'><a href="/print_settings">Print Setting</a></li>
                  <li class='black'><a href="/lab_categories">Report Master</a></li>
                  <li class='black'><a href="/prescription_references">Ref. Centers</a></li>
                  <li class='black'><a href="/sample_collection_centers">Sample Collection Centers</a></li>
                  <li class='black'><a href="/saved_templates">Saved Templates</a></li>
                  <li class='black'><a href="/specimen_masters">Specimen masters</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <div class='pull-right navbar-text'>
            <ul class='nav border-none'>
              <li class='dropdown'>
                <a class='dropdown-toggle white-color' data-toggle='dropdown' href='#'>
                  <i class='fa fa-cog fa-white'></i>
                  <span class='small-font'>
                    New User
                    (admin)
                  </span>
                  <b class='caret'></b>
                </a>
                <ul class='dropdown-menu'>
                  <li class='black'><a href="/profiles/2">Profile</a></li>
                  <li class='black'><a href="/sms_stats">My plan</a></li>
                  <li class='black'><a href="/users/sign_out"><i class='fa fa-off fa-white'></i>  Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <div class='clearfix'></div>
        </div>
      </div>
    </div>  