<div id='content-div'>
  <div class='inner_wrapper'>
    <div class="hero-text">
       <h2>Presciptions</h2>
       <p class="lead"></p>
       {{if .flash.error}}
      <blockquote>{{.flash.error}}</blockquote>
      {{end}}

      {{if .flash.notice}}
      <blockquote>{{.flash.notice}}</blockquote>
      {{end}}
    </div>
  

  
    <table class="table table-bordered table-hover prescriptions">
      <thead>
        <tr class="trheader">
          <th>Prescribed Date </th>
          <th> Patient </th>
          <th> Reg / Lab No </th>
          <th> Prescription Type </th>
          <th> Venue </th>
          <th> Prescribed By </th>
          <th> Ref. Center </th>
          <th> Tests </th>
          <th> Bill </th>
          <th>Details</th>
        </tr>
      </thead>
      <tbody>
        {{range $record := .records}}
          <tr>
            <td>{{$record.Date}}}</td>
            <td class="patient">
              {{$record.Patient.Patient_name_with_initial}}
              </br>
              <span class="age">{{$record.Patient.Check_for_age}}</span>
              {{if $record.Patient.Gender}}
                {{if $record.Patient.Gender | male}}
                  <img src="/static/images/male.png" alt="Male">
                {{else}}
                  <img src="/static/images/female.png" alt="Female">
                {{end}}
              {{end}}
              {{if $record.Patient.Mobile}}
                {{$record.Patient.Mobile}}
              {{end}}
            </td>
            <td>
              {{if $record.LabNo}}
                {{$record.Patient.PatientNo}}
              {{else}} 
                {{$record.Patient.PatientNo}} 
                {{$record.LabNo}} 
              {{end}}      
            </td>
            <td>
              {{$record.Ip_op_with_detail}}
            </td>
            <td class="wrapped">{{$record.Venue.Name}}</td>
            <td class="wrapped">{{$record.Doctor.Name_with_degree}}</td>
            <td class="wrapped">{{$record.ReferredBy.Name}}</td>
            <td>
              <a class="label label-info" data-content={{$record.Extra_lab_test_details | pop_up}} data-placement="left" data-trigger="hover" rel="popover">Lab Tests</a>
            </td>
            <td></td>
            <td></td>
          </tr>
        {{end}}
      </tbody>
      <tfoot>
        <tr>
          <td colspan="4"><a href="{{urlfor "ManageController.Add"}}" title="add new article">add new article</a></td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>