<div id="content-div">
  <div class="inner_wrapper">
    <div class="hero-text">
      <h1>Reports</h1>
      <p class="lead"></p>
      {{if .flash.error}}
      <blockquote>{{.flash.error}}</blockquote>
      {{end}}

      {{if .flash.notice}}
      <blockquote>{{.flash.notice}}</blockquote>
      {{end}}
    </div>
 
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>Id</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody>
        {{range $record := .records}}
        <tr>
          <td>{{$record.Id}}</td>
          <td>{{$record.CreatedAt}}</td>
        </tr>
        {{end}}
      </tbody>
      <tfoot>
        <tr>
          <td colspan="4"><a href="{{urlfor "ManageController.Add"}}" title="add new article">add new article</a></td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>