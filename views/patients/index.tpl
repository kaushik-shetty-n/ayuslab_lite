


<div id='content-div'>
  <div class='inner_wrapper'>
    <div class="hero-text">
       <h2>Patients</h2>
       <p class="lead"></p>
       {{if .flash.error}}
      <blockquote>{{.flash.error}}</blockquote>
      {{end}}

      {{if .flash.notice}}
      <blockquote>{{.flash.notice}}</blockquote>
      {{end}}
    </div>
  

  
    <table id="patients" class="table table-bordered table-hover">
      <thead>
        <tr class="trheader">
          <th>Name</th>
          <th>Patient No</th>
          <th>Age</th>
          <th>Mobile</th>
        </tr>
      </thead>
      <tbody>
        {{range $record := .records}}
        <tr>
          <td>{{$record.Id}}</td>
          <td>{{$record.Name}}</td>
          <td>{{$record.Dob}}</td>
          <td>{{$record.Mobile}}</td>
        </tr>
        {{end}}
      </tbody>
      <tfoot>
        <tr>
          <td colspan="4"><a href="{{urlfor "ManageController.Add"}}" title="add new article">add new article</a></td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>