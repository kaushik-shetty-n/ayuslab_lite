 function compute(st) {
  var common_year_days_in_month = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
  var start_date = st;
  var end_date = new Date();

  if(start_date > end_date) {
    s = start_date;
    start_date = end_date;
    end_date = s;

  }


  start_date_year = start_date.getFullYear();
  start_date_month =  start_date.getMonth()+1;
  start_date_day = start_date.getDate() ;

  end_date_year = end_date.getFullYear();
  end_date_month = end_date.getMonth()+1;
  end_date_day = end_date.getDate() ;

  day_difference = end_date_day - start_date_day;


  
  if (day_difference < 0) {
      day_difference = common_year_days_in_month[start_date_month-1] - start_date_day + end_date_day;
      end_date_month -= 1;
  }

  if (end_date_month < 0) {
    end_date_month += 12;
    end_date_year -= 1;
  }



  month_difference = end_date_month - start_date_month;

  if (month_difference < 0) {
    month_difference += 12;
    end_date_year -= 1;
  }

  year_difference = end_date_year - start_date_year    ;
 


  $('#patient_age_year').val(year_difference);
  $('#patient_age_month').val(month_difference);
  $('#patient_age_day').val(day_difference);



}

 
