jQuery(document).ready(function() {
  total_calculation();
  $("body").on("change", "input.bill_amount",function() {
    total_calculation();
  });
  $("body").on("change", "input.amount_paid" ,function() {
    total_calculation(); 
    var totalafterdiscount = total_after_discount();
    var amountpaid = amount_paid();
	  if(amountpaid > totalafterdiscount) {
	    var c = totalafterdiscount - amountpaid + parseFloat($(this).val());
	    $(this).val(c);
	    $("span#pending").html(c);
	  }  
  });
  $("body").on("change", "input#bill_discount",function() {
    if($(this).val() == ""){
      $(this).val('0.0');
    }
    total_calculation();
  });
  $("body").on("change", "input.bill_refund",function() {
    if($(this).val() == ""){
      $(this).val('0.0');
    }
    total_calculation();
  });  
  $("body").on("change", "input#bill_percent_discount",function() {
    if($(this).val() == ""){
      $(this).val('0.0');
    }
    total_calculation();
  });
  $("body").on("click", ".add-bill-item-link",function() {
    bill_autocomplete();
    $('.lab_test_autocomplete:last').focus();
  });  
});



function bill_amount() {
  var total = 0.0;
  $("input.bill_amount").each(function(index) {
    total += parseFloat($(this).val());
	});
	return total;  
}

function amount_paid() {
  var paid = 0.0;
  $("input.amount_paid").each(function(index) {
    paid += parseFloat($(this).val());
	});
	return paid;  
}

function percentage() {
  var percentage = 0;
  percentage = parseFloat($("#bill_percent_discount").val());
  return percentage
}

function discount() {
  var return_percentage = percentage();
  //console.log(return_percentage);
  
  if(return_percentage == 0 ){
    var discount =  parseFloat($("#bill_discount").val());
  }else{
    var total_bill_amunt = bill_amount();
    var discount = total_bill_amunt*(return_percentage/100);
    $("#bill_discount").val(discount.toFixed(2));
  }
  //var discount =  parseFloat($("#bill_discount").val());
  return discount;
}

function refund_amount() {
  var rf = parseFloat($(".bill_refund:first").val());

  if(isNaN(rf)){
   return  0.0;
  }else{
    return rf;
  }
}

function total_calculation(){
  var total = bill_amount();
  var dis = discount();


	if(dis > total) {
	  dis = total;
	  $("#bill_discount").val(total);
	}
	total -= dis;
  $("span#total").html(total);  
  $("span#total_price").html(total);  
  pending_amount();
}

function total_after_discount() { 
  var total = bill_amount();
  var dis = discount();
  return	total -= dis;
}


function pending_amount() {
  var totalafterdiscount = total_after_discount();

  var amountpaid = amount_paid();
  var refund = refund_amount();
 	
  $("span#pending").html((totalafterdiscount - amountpaid + refund).toFixed(2));
}


function delete_bill_item(current_link) {
  var row = $(current_link).parents('tr');
  d = $(row).find('.destroy_bill_item:first').attr('id');
  delete_bill_item_from_val(d);
  $(current_link).parent().parent().remove();
  total_calculation();
}

function delete_bill_item_from_val (d) {
  var t = d.split('bill_bill_items_attributes_')[1].split("__destroy")[0];
  var ff = "<input id='bill_bill_items_attributes_" + t + "__destroy'  type='hidden' value='1' name='bill[bill_items_attributes][" + t + "][_destroy]'>";
  $('form.bill').append(ff);
}

function bill_autocomplete() {
  $('.lab_test_autocomplete').autocomplete({ 
    minLength: 2,
    autoFocus: true,
    source: billjson,
    source: function(request, response) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response($.grep(billjson, function(item){return matcher.test(item.label); }) );
    },
    select: function( event, ui ) {
      var d = $(this).attr('id').split('bill_bill_items_attributes_')[1].split('_bill_item_name')[0];
      $('#bill_bill_items_attributes_' + d + '_amount').val(ui.item.price);
      if(ui.item.type == "LabTest"){
        $('#bill_bill_items_attributes_' + d + '_lab_test_id').val(ui.item.id);
      }else if (ui.item.type == "BillItemMaster"){
        $('#bill_bill_items_attributes_' + d + '_bill_item_master_id').val(ui.item.id);
      }
      total_calculation();
    } 

  });  
}


function bill_time_selector () {
  $("#bill_billed_time").datetimepicker({dateFormat: 'dd-mm-yy', changeMonth: true, changeYear: true, timeFormat: 'HH:mm:ss' });
}


function add_bill_item_to_bill(type, name, id, price) {
  length = new Date().getTime();
  s = ""
  s += "<tr>" ;
    s += "<td>" ;
      s += "<div class='input string required'>"
        s += "<input type='text' value=\"" + name + "\" size='50' required='required' name='bill[bill_items_attributes][" + length + "][bill_item_name]' maxlength='255' id='bill_bill_items_attributes_"+ length +"_bill_item_name' class='string required lab_test_autocomplete ui-autocomplete-input' autocomplete='off'>"
      s += "</div>"
      if(type == "Package"){
        s += "<input type='hidden' class='package_" +id+ "' value='" + id + "' name='bill[bill_items_attributes][" + length + "][package_id]' id='bill_bill_items_attributes_"+ length +"_package_id' class='hidden'>"
      }else if(type == "LabTest"){
        s += "<input type='hidden' class='lab_test_" +id+ "' value='" + id + "' name='bill[bill_items_attributes][" + length + "][lab_test_id]' id='bill_bill_items_attributes_"+ length +"_lab_test_id' class='hidden'>"
      }else{
        s += "<input type='hidden' class='bill_item_master_" +id+ "' value='" + id + "' name='bill[bill_items_attributes][" + length + "][bill_item_master_id]' id='bill_bill_items_attributes_"+ length +"_bill_item_master_id' class='hidden'>"
      }  
    s += "</td>"
    s += "<td>"
      s += "<div class='input numeric decimal optional'>"
        s += "<input type='number' value='" + price + "' step='any' size='50' name='bill[bill_items_attributes][" + length + "][amount]' id='bill_bill_items_attributes_"+ length +"_amount' class='numeric decimal optional input-mini bill_amount'>"
      s += "</div>"
    s += "</td>"
    s += "<td>"
      s += "<input type='hidden' value='false' name='bill[bill_items_attributes][" + length + "][_destroy]' id='bill_bill_items_attributes_"+ length +"__destroy' class='hidden  destroy_bill_item'>"
    s += "</td>"
  s += "</tr>"
  $("table#bill_items").append(s);
}


function remove_bill_item_from_bill(type, id) {
  if(type == "Package"){
    $("table#bill_items .package_" +id).parents("tr").find(".destroy_bill_item:last").val(1);
    $("table#bill_items .package_" +id).parents("tr").find(".bill_amount:last").removeClass('bill_amount');
    $("table#bill_items .package_" +id).parents("tr").hide();
  }else if(type == "LabTest"){
    $("table#bill_items .lab_test_" +id+":last").parents("tr").find(".destroy_bill_item:last").val(1);
    $("table#bill_items .lab_test_" +id+":last").parents("tr").find(".bill_amount:last").removeClass('bill_amount');
    $("table#bill_items .lab_test_" +id+":last").parents("tr").hide();
  }else{
    $("table#bill_items .bill_item_master_" +id).parents("tr").find(".destroy_bill_item:last").val(1);
    $("table#bill_items .bill_item_master_" +id).parents("tr").find(".bill_amount:last").removeClass('bill_amount');
    $("table#bill_items .bill_item_master_" +id).parents("tr").hide();
  }  
}


function add_master_bill_item_to_bill() {
  length = new Date().getTime();
  s = ""
  s += "<tr>" ;
    s += "<td>" ;
      s += "<div class='input string required'>"
        s += "<input type='text' value='' size='50' required='required' name='bill[bill_items_attributes][" + length + "][bill_item_name]' maxlength='255' id='bill_bill_items_attributes_"+ length +"_bill_item_name' class='string auto_complete_master' autocomplete='off'>"
      s += "</div>"

      s += "<input type='hidden' class='bill_item_master' value='' name='bill[bill_items_attributes][" + length + "][bill_item_master_id]' id='bill_bill_items_attributes_"+ length +"_bill_item_master_id' class='hidden'>"
 
    s += "</td>"
    s += "<td>"
      s += "<div class='input numeric decimal optional'>"
        s += "<input type='number' value='0.0' step='any' size='50' name='bill[bill_items_attributes][" + length + "][amount]' id='bill_bill_items_attributes_"+ length +"_amount' class='numeric decimal optional input-mini bill_amount'>"
      s += "</div>"
    s += "</td>"
    s += "<td>"
      s += "<input type='hidden' value='false' name='bill[bill_items_attributes][" + length + "][_destroy]' id='bill_bill_items_attributes_"+ length +"__destroy' class='hidden  destroy_bill_item'>"
    s += "<a onclick='delete_bill_item_from_bill($(this));return false;' class='btn btn-danger btn-mini' href='#'>x</a>";
    s += "</td>"
  s += "</tr>"
  $("table#bill_items").append(s);
  auto_complete_master();
}

function auto_complete_master() {
  $('.auto_complete_master:last').focus();
  $('.auto_complete_master').autocomplete({ 
    minLength: 1,
    autoFocus: true,
    source: master_bill_items_json,
    source: function(request, response) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response($.grep(master_bill_items_json, function(item){return matcher.test(item.label); }) );
    },
    select: function( event, ui ) {
      var d = $(this).attr('id').split('bill_bill_items_attributes_')[1].split('_bill_item_name')[0];
      $('#bill_bill_items_attributes_' + d + '_amount').val(ui.item.price);
      if(ui.item.type == "LabTest"){
        $('#bill_bill_items_attributes_' + d + '_lab_test_id').val(ui.item.id);
      }else if (ui.item.type == "BillItemMaster"){
        $('#bill_bill_items_attributes_' + d + '_bill_item_master_id').val(ui.item.id);
      }
      total_calculation();
    } 
  });    
}


function delete_bill_item_from_bill(current_link) {
  var tr =  $(current_link).parents("tr");
  if(new_bill){
    $(tr).remove();
  }else{
    $(tr).find(".destroy_bill_item:last").val(1);
    $(tr).find(".bill_amount:last").removeClass('bill_amount');
    $(tr).hide();    
  }
  total_calculation();
}