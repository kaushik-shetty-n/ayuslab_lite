function labtestautocomplete_for_packages(lab_tests) {
  var json = lab_tests;
  $('#lab_testname_for_packages').autocomplete({ 
    minLength: 2,
    autoFocus: true,
    source: json,
    source: function(request, response) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response($.grep(json, function(item){return matcher.test(item.label); }) );
    },
    select: function( event, ui ) {
      if(ui.item.type == "LabTest"){
        $("input[type='checkbox'][class='test-test-id-"+ui.item.id+"']:first").prop("checked", true);
        lab_test_added_array.push(ui.item.id);
      }else{
        $("input[type='checkbox'][class='bill-item-master-id-"+ui.item.id+"']:first").prop("checked", true);
      }
      var d = "<div class='alert alert-success' style='height: 15px'>";
      d += ui.item.label ;
      d += " selected </div>";
      add_lab_test_prescribed_lists_from_packages(ui.item.label, ui.item.id, ui.item.type);

      $('#alert-area').html(d).show().delay(1000).fadeOut("slow", function () { $(this).hide(); });      
      $(this).val('');
      return false;
    }
  });  
}

function add_lab_test_prescribed_lists_from_packages (test_name, id, t) {
  if(t == "LabTest"){
    d = "label-info lab-test-added-" + id;
  }else{
    d = "label-bill-item-master bill-item-added-" + id;
  }
  var s = "<span class='label " + d +"'> " + test_name ;
  if(t == "LabTest"){
    s += "<a class='danger-color' onclick=\"remove_lab_test_from_packages('"+id+"'); return false;\" href='#'> x</a>";
  }else{
    s += "<a class='danger-color' onclick=\"remove_bill_master_from_prescription('"+id+"'); return false;\" href='#'> x</a>";
  }
  s += "</span>";
  $('#lab-test-prescribed-lists').append(s);
  enable_or_disable_autocomplete_options_for_packages();
}

function add_or_remove_lab_tests_packages(ch, lab_test_name, value, t) {
  var checked = $(ch).prop('checked');
  if(checked){
    add_lab_test_prescribed_lists_from_packages(lab_test_name, value, t);
    lab_test_added_array.push(value);
    enable_or_disable_autocomplete_options_for_packages();
  }else{
    $('.lab-test-added-'+ value).remove();
    remove_lab_test_from_array(value);
  }  
}

function remove_lab_test_from_packages(lab_test_id) {
  $('span.lab-test-added-'+ lab_test_id).remove();
  $("input[type='checkbox'][class='test-test-id-"+lab_test_id+"']:first").prop("checked", false);
  remove_lab_test_from_array(lab_test_id);
}

function enable_or_disable_autocomplete_options_for_packages() { // disble lab test once added to prescribed test
  autocomplete_array = jQuery.grep(venue_bill_items, function(element) {
    return jQuery.inArray(element.id, lab_test_added_array ) == -1;
  });
  $('#lab_testname_for_packages').autocomplete('option', 'source', autocomplete_array);  
}

function remove_lab_test_from_array(lab_test_id) {
    jQuery.each( lab_test_added_array, function( i, array ) {
      if(array == lab_test_id){
        lab_test_added_array.splice(i, 1);
        return false;
      }
    }); 
  enable_or_disable_autocomplete_options_for_packages();

}