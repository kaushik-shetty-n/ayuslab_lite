// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require jquery-ui-1.10.3.custom
//= require jquery-ui-timepicker-addon.js
//= require wysihtml5-0.3.0.js
//= require bootstrap-wysihtml5
//= require select2
//= require prescription
//= require bills
//= require bootstrap-modalmanager.js
//= require bootstrap-modal.js
//= require bootstrap-multiselect.js
//= require jquery.remotipart
//= require common
//= require age_calculation_plugin.js
//= require summernote.min
//= require package


jQuery(document).ready(function() {

  patient_datepicker();
  patient_age_popover();
  $("#prescription_doctor_id, select#doctors, #prescription_referred_by_id, .doctor_list, .select2_selector, #saved_template_lab_test_id").select2();
  $('#accordion2').collapse({ toggle: false });
  $('#billtab a:first').tab('show');
  $("#q_created_at_date_equals, #search_date_eq, #reference_range_age_to, #reference_range_age_from, .datepicker").datepicker({dateFormat: 'dd-mm-yy',changeMonth: true, changeYear: true });
  $(".datetime-picker-ampm").datetimepicker({ampm: true, dateFormat: 'dd-mm-yy',changeMonth: true, changeYear: true, timeFormat: 'hh:mm tt' });
  $("#q_created_at_eq, #q_date_eq").datepicker({dateFormat: 'dd-mm-yy',changeMonth: true, changeYear: true, maxDate: 0 });
  lab_test_parameter_sort();
  jquery_text_editor();
  package_select_fn();
  datetime_selector();
  patient_age_input_calculation();
  conditional_parameter_select_fun();
  conditional_parameter_select_preview_fun();
  bill_time_selector();
  receipt_search_form_submit_function();

  $('.patient_name_autocomplete').focusout(function() {
    $('#registered_patient_found').hide();
    if($(this).val().length > 2){
      $.ajax({  data:'term='+ $(this).val(), 
        dataType:'script', 
        type:'get', 
        url: '/bill_first_modules/autocomplete?phone_search=false'
      });
    }else{
      $('#registered_patient_found').hide();
    }    
  });

  $('.patient_phone_autocomplete').focusout(function() {
    $('#registered_mobile_found').hide();
    if($(this).val().length > 2){
      $.ajax({  data: 'term='+ $(this).val(), 
        dataType:'script', 
        type:'get', 
        url: '/bill_first_modules/autocomplete?phone_search=true'
      });
    }else{
      $('#registered_mobile_found').hide();
    }    
  });


  $('#patient_initial').on('change', function() {
    var optionSelected = $(this).find("option:selected");
    switch (optionSelected.val()){
      case 'Mr':
        $("#patient_gender").val('male');
        break;
      case 'Master':
        $("#patient_gender").val('male');
        break;
      case 'Miss':
        $("#patient_gender").val('female');
        break;
      case 'Mrs':
        $("#patient_gender").val('female');
        break;
      case 'Ms':
        $("#patient_gender").val('female');
        break;
      case 'Baby':
        $("#patient_gender").val('');
        break;
      case 'Baby of':
        $("#patient_gender").val('');
        break;
      case 'Dr':
        $("#patient_gender").val('');
        break;
      case 'Pet':
        $("#patient_gender").val('other');
        break;
      case 'Sister':
        $("#patient_gender").val('female');
        break;
      case '':
        $("#patient_gender").val('');
        break;
    }    
  });


   
});

function do_you_want_to_add_all_lab_tests_in_test_classification(check_box_clicked) {
  var checked = $(check_box_clicked).prop('checked');
  if(checked){
    var confirmed = confirm("Do you want to add all lab tests in this category?");
    if (confirmed==true){
      var v = $(check_box_clicked).val();
      var css_class = "lab_category_" + v;
      $(".lab_test").each(function () { 
          console.log("ddddddddd")
          if ($(this).hasClass(css_class)){
            console.log(css_class)
            $(this).prop("checked", true);
          }
        })
      //$("input:checkbox[class='"+css_class+"']").prop("checked", true);
    }
  }else{
    var css_class = "lab_category_" + $(check_box_clicked).val();
    $(".lab_test").each(function () { 
          console.log("ddddddddd")
          if ($(this).hasClass(css_class)){
            console.log(css_class)
            $(this).prop("checked", false);
          }
        })
    //$("input:checkbox[class='"+css_class+"']").prop("checked", false);
  }
}

function add_all_receipts_on_click() {
  //alert('Hi')
  $('a.add_this').each(function(){
     $(this).click();
  });

}

function check_for_all_lab_tests_test_classification(check_box_clicked) {
  var checked = $(check_box_clicked).prop('checked');
  if(checked){
    $("input[class='lab_test_in_test_classification']:checked").prop('checked', true);

  }else{
    $("input[class='lab_test_in_test_classification']:checked").prop('checked', false);
  }
}


function check_for_allow_commission_doctors(check_box_clicked) {
  var checked = $(check_box_clicked).prop('checked');
  if(checked){
    $('#test_classification-list').show();
    $("input[class='test_classification_checkbox']").prop('checked', true);
  }else{
    $('#test_classification-list').hide();
    $("input[class='test_classification_checkbox']:checked").prop('checked', false);
  }
}


function datetime_selector() {
  $(".datetime-picker").datetimepicker({dateFormat: 'dd-mm-yy',changeMonth: true, changeYear: true, timeFormat: 'HH:mm:ss' });
}

function conditional_parameter_select_fun() {
  $("body").on("change", "select.conditional-parameter-select",function() {
    var select_id = $(this).attr('id');
    //var id_attr = select_id.split("report_report_values_attributes_")[1].split("_conditional_result")[0];
    //$('select#report_report_values_attributes_' + id_attr + '_result').val($(this).find('option:selected').text());
  
    $('select#'+ select_id.replace("_conditional", "")).val($(this).find('option:selected').text());
    $('.default_loading_false').hide();
    $('.default_loading_false').find('.default_loading_input').val(false);
    $('.result_option_'+$(this).val()).show();
    $('.result_option_' + $(this).val()).find('.default_loading_input').val("true");
  });
}

function conditional_parameter_select_preview_fun() {
  $("body").on("change", "select.conditional-parameter-select-preview",function() {
    var select_id = $(this).attr('id');
    var id_attr = select_id.split("report_")[1].split("conditional_result")[0];
    $('select#report_' + id_attr + 'result').val($(this).find('option:selected').text());
    $('.default_loading_false').hide();
    $('.default_loading_false').find('.default_loading_input').val(false);
    $('.result_option_'+$(this).val()).show();
    $('.result_option_' + $(this).val()).find('.default_loading_input').val("true");
  });
}


function patient_datepicker() {
  $("#patient_dob").datepicker({dateFormat: 'dd-mm-yy',changeMonth: true, changeYear: true, minDate: "-90Y" ,maxDate: 0, yearRange: "c-90:c+1", onSelect: function(dateText, dp) {  $("span#update-age-button").show(); }  });
}

function package_select_fn() {
  $('.package_select').multiselect(
    {
      enableFiltering: true, nonSelectedText: "Select Package",
      enableCaseInsensitiveFiltering: true,
      onChange:function(element, checked){
        if(checked) { 
          package_selected(element.val()); 
          add_package_to_bill_items(element.val());
          add_package_lab_tests_to_specimen(element.val());
        }else { un_package_selected(element.val()); remove_package_to_bill_items(element.val());}
      }, 
      buttonText: function(options, select) {
        if (options.length == 0) {
          return 'Select Package <b class="caret"></b>';
        }
        else if (options.length > 2) {
            return options.length + ' Packages selected <b class="caret"></b>';
        }
        else {
            var selected = '';
            options.each(function() {
                selected += $(this).text() + ', ';
            });
            return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
        }
    }
  });
}
 
function patient_age_popover() {
  $('a[rel=popover]').popover({ html: 'true'})
};

function calculate_dob_for_preview(){
  var year = parseInt($('#find_dob_year').val());
  var month = parseInt($('#find_dob_month').val());

  var day = parseInt($('#find_dob_day').val());
  var c = $('#find_dob_type').val();
  var t = new Date();
  if(!isNaN(year)){
    t.setFullYear(t.getFullYear() - year);
  }
  if(!isNaN(month)){
    t.setMonth(t.getMonth() - month); 
  }
  if(!isNaN(day)){
    t.setDate(t.getDate() - day);
  }

  $("#patient_dob").val(t.getDate()+"-"+(t.getMonth() + 1)+"-"+t.getFullYear());

  
  popover_hide();
  return false;
}


function popover_hide() {
  $('.popover').hide();
}

function jquery_text_editor () {
  //$('.html-editor').wysihtml5();
  $('.html-editor').summernote({
		width: "595px"
	});
	
	$('.note-help').remove();
  //$('.html-editor').redactor({ fixed: true })  
}  


function create_dispatch_report() {
  if(bill_created) {
    var report_checked = $("input[name='report[]']:checked").getCheckboxVal();
    if(report_checked.length == 0){
      alert('please select report');
    }else{
       $.ajax({data: 'report_ids='+ report_checked , dataType:'script',   type:'get', url: '/report_dispatches/new' });
    }
  }else{
    alert("Please create Bill First.")
  }
}

  
jQuery.fn.getCheckboxVal = function(){
    var vals = [];
    var i = 0;
    this.each(function(){
        vals[i++] = jQuery(this).val();
    });
    return vals;
}


function delete_function(link) {
  $(link).parent().parent().parent().parent().find('input[type=hidden]').val('1'); 
  $(link).parent().parent().parent().parent().hide();
}

function labtest_sort_in_report() {
  $('#lab_test_sort').sortable({cursor: 'move',
        update: function(event, ui) {
          var order = $(this).sortable('toArray').toString();
          $.ajax({
            data:'order='+ order, 
            dataType: 'script', 
            type:'get', 
            url: '/reports/sort',
         });
        }
  });
}



function labtest_search_autocomplete(lab_tests) {
  var json = $.parseJSON(lab_tests);
  $('.test-name-autocomplete').autocomplete({ 
    minLength: 2,
    autoFocus: true,
    source: json,
    source: function(request, response) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response($.grep(json, function(item){return matcher.test(item.label); }) );
    }
  });  
}

function package_search_autocomplete(packages) {
  var json = $.parseJSON(packages);
  $('.package-name-autocomplete').autocomplete({ 
    minLength: 2,
    autoFocus: true,
    source: json,
    source: function(request, response) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response($.grep(json, function(item){return matcher.test(item.label); }) );
    }
  });  
}



function labcategory_search_autocomplete(lab_categories) {
  var lab_categories = $.parseJSON(lab_categories);
  $('.lab-category-name-autocomplete').autocomplete({ 
    minLength: 2,
    autoFocus: true,
    source: lab_categories,
    source: function(request, response) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response($.grep(lab_categories, function(item){return matcher.test(item.label); }) );
    }
  });  
}



function masterlabtestautocomplete(lab_tests) {
  var json = $.parseJSON(lab_tests);
  $('#master_lab_testname').autocomplete({ 
    minLength: 1,
    autoFocus: true,
    source: json,
    source: function(request, response) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response($.grep(json, function(item){return matcher.test(item.label); }) );
    },
    select: function( event, ui ) {
      if(ui.item.added) {
         alert(ui.item.label + " All ready added") ;
      }else{
        var c = $('input[value="'+ui.item.master_lab_test_id+'"]').prop('checked');
        if(!c) {
          $('input[value="'+ui.item.master_lab_test_id+'"]').trigger('click');
        }  
        var d = "<div class='alert alert-success' style='height: 15px'>";
        d += ui.item.label ;
        d += " selected </div>";
        $('#alert-area').html(d).show().delay(1000).fadeOut("slow", function () { $(this).hide(); });  
      } 
      return false;     
    } 
  });  
}



function lab_test_parameter_sort() {
  $("#lab_test_settings").sortable({cursor: 'move',
        update: function(event, ui) {
          var order = $(this).sortable('toArray').toString();
          $.ajax({
            data:'order='+ order, 
            dataType:'script', 
            type:'get', 
            url: '/lab_categories/'+lab_category_id+'/lab_tests/'+lab_test_id+'/sort'
         });
        }
    });
}


function do_you_want_to_add_all_lab_tests(check_box_clicked) {
  var checked = $(check_box_clicked).prop('checked');
  if(checked){
    var confirmed = confirm("Do you want to add all lab tests in this category?");
    if (confirmed==true){
      var v = $(check_box_clicked).val();
      var css_class = "master_lab_category_" + v;
      $("input:checkbox[class='"+css_class+"']").prop("checked", true);
    }
  }else{
    var css_class = "master_lab_category_" + $(check_box_clicked).val();
    $("input:checkbox[class='"+css_class+"']").prop("checked", false);
  }
}



function check_for_lab_category_ticked(lab_check_box_clicked) {
  var c = $(lab_check_box_clicked).attr('class').split('master_lab_category_')[1];
  var lab_category = $(".master-lab-category-" + c + ":first");
  var f = lab_category.attr('disabled')
  var checked = lab_category.prop('checked');
  if(typeof f == 'undefined'){
    var disabled = false;
  }else{
    var disabled = true;
  }
  
  if(!(checked || disabled)) {
    var  message = 'selected corresponding category ';
    $.each(master_lab_categories_json, function(index, master_lab_category) {
     if(c == master_lab_category.id) {
        message += master_lab_category.label;
        return false;
      }
    });

    alert(message);
    $(".master-lab-category-" + c + ":first").attr('checked', true);
  }
}

function sample_collected_at_datetime_picker () {
  $("#prescription_sample_collected_at").datetimepicker({dateFormat: 'dd-mm-yy',changeMonth: true, changeYear: true, timeFormat: 'HH:mm:ss' });
}

function check_mobile_no_entered(sms_check_box) {
  var checked = $(sms_check_box).prop('checked');
  if(checked){
    if($('#patient_mobile').val() == ""){
      alert('Please enter mobile no')
      sms_check_box.attr('checked', false);
    }
  }
}

function check_email_entered(email_check_box) {
  var checked = $(email_check_box).prop('checked');
  if(checked){
    if($('#patient_email').val() == ""){
      alert('Please enter email')
      email_check_box.attr('checked', false);
    }
  }
}

function check_email_entered_doctor(email_check_box) {
  var checked = $(email_check_box).prop('checked');
  if(checked){
    if($('#doctor_email').val() == ""){
      alert('Please enter email')
      email_check_box.attr('checked', false);
    }
  }
}

function check_email_entered_ref_center(email_check_box) {
  var checked = $(email_check_box).prop('checked');
  if(checked){
    if($('#prescription_reference_email').val() == ""){
      alert('Please enter email')
      email_check_box.attr('checked', false);
    }
  }
}

function checkall_or_uncheckall(check_box_clicked) {
  var checked = $(check_box_clicked).prop('checked');
  if(checked){
    $("input:checkbox[class='bill-ids-select']").prop("checked", true);
    $('#clear-bill-link').removeClass('hide');
  }else{
    $("input:checkbox[class='bill-ids-select']").prop("checked", false);
    $('#clear-bill-link').addClass('hide');
  }
}


function bill_id_select_clicked(check_box_clicked) {
  var checked = $(check_box_clicked).prop('checked');
  if(checked){
    $('#clear-bill-link').removeClass('hide');
  }else{
    if($("input[class='bill-ids-select']:checked").length == 0){
      $('#clear-bill-link').addClass('hide');
    }
    $('#check_all_and_uncheckall').prop("checked", false);
  }
}

function clear_bills() {
  var allVals = [];
  $("input[class='bill-ids-select']:checked").each(function() {
       allVals.push($(this).val());
  });

  $.ajax({
      data:'bill_ids='+ allVals, 
      dataType:'script', 
      type:'post', 
      url: '/billing_name_clearances'
  });
}

function highlight_checkbox_clicked (cb) {
  var checked = $(cb).prop('checked');
  var row = $(cb).parents('.tr');
  if(checked){
    $(row).find(".highlight_check_box").val(true);
  }else{
    $(row).find(".highlight_check_box").val(false);
  }
}




function patient_age_input_calculation() {
  $("body").on("change", "input#patient_age_year" ,function() {
    var year_value = parseInt($(this).val());
    if(year_value > 150){
      alert('please enter valid years');
      $(this).val('');
    }else{
      calculate_dob();
    } 
  });
  $("body").on("change", "input#patient_age_month" ,function() {
     calculate_dob();
  });
  $("body").on("change", "input#patient_age_day" ,function() {
     calculate_dob();
  });    
}


function calculate_dob(){
  var year = $('#patient_age_year').val();
  var month = $('#patient_age_month').val();
  var day = $('#patient_age_day').val();
  
  if(year == "" && month == "" && day == "") {
    $("#patient_approximate_dob").val("");
  }else {
    var year = parseInt(year);
    var month = parseInt(month);
    var day = parseInt(day);

    var t = new Date();
    if(!isNaN(year)){
      t.setFullYear(t.getFullYear() - year);
    }
    if(!isNaN(month)){
      t.setMonth(t.getMonth() - month); 
    }
    if(!isNaN(day)){
      t.setDate(t.getDate() - day);
    }
    $("#patient_approximate_dob").val(t.getDate()+"-"+(t.getMonth() + 1)+"-"+t.getFullYear());

  }  
  $("span#update-age-button").show();
  return false;
}


function call_compute_function(date) {
  var c = date.split("-");
  compute(new Date(c[0],c[1]-1,c[2]));
}

function lab_categories_sort() {
  $("#lab-categories-sort").sortable({cursor: 'move',
    update: function(event, ui) {
      var order = $(this).sortable('toArray').toString();
      $.ajax({
        data:'order='+ order, 
        dataType:'script', 
        type:'get', 
        url: '/lab_categories/sort_update'
     });
    }
  });
}

function lab_category_lab_test_sort() {
  $(".lab_category_lab_test_sort").sortable({cursor: 'move',
    update: function(event, ui) {
      var order = $(this).sortable('toArray').toString();
      $.ajax({
        data:'order='+ order, 
        dataType:'script', 
        type:'get', 
        url: '/lab_categories/0/lab_tests/sort_lab_tests'
     });
    }
  });
}
function venue_selected(current_venue) {
  if(current_venue.val() != ""){ 
    $("form#venue-selected").submit();
  }else{
    $('#venue_doctors, #venue_lab_categories_and_lab_tests').html('');
  }
}


function check_all_venue_doctors(check_box_clicked) {
  var checked = $(check_box_clicked).prop('checked');
  if(checked){
    $("input:checkbox[class='select-all-doctors']").prop("checked", true);
    $('#import-doctors-other-venue').show();
  }else{
    $("input:checkbox[class='select-all-doctors']").prop("checked", false);
    $('#import-doctors-other-venue').hide();
  }
}



function doctor_import_clicked(check_box_clicked) {
  var checked = $(check_box_clicked).prop('checked');
  if(checked){
    $('#import-doctors-other-venue').show();
  }else{
    if($("input[class='select-all-doctors']:checked").length == 0){
      $('#import-doctors-other-venue').hide();
    }
    $('#check_all_doctors').prop("checked", false);
  }
}

function import_doctors_from_other_venue() {
  var allVals = [];
  $("input[class='select-all-doctors']:checked").each(function() {
    allVals.push($(this).val());
  });
  $.ajax({
      data:'doctor_ids='+ allVals, 
      dataType:'script', 
      type:'get', 
      url: '/import_doctors/import'
  });
  $('#import-doctors-other-venue').hide();
}


function change_select_textbox(linked, value) {
  var selector = $(linked).parents('td').find('.change-select-textbox');
  var selector_id = selector.attr('id')
  var selector_name = selector.attr('name')
 
  //var no = selector_id.split("report_report_values_attributes_")[1].split("_result")[0];
  var text_box = "<span> <input id=" + selector_id + " type='text' value='" + value + "' name=" + selector_name + " </span>";
  $("select#" + selector_id).remove(); 
  $(linked).parents('td').find('.change-select-textbox-div').html(text_box);
  $(linked).hide();
}


function outsourced_labtestautocomplete(lab_tests) {
  var json = $.parseJSON(lab_tests);
  $('#lab_testname').autocomplete({ 
    minLength: 2,
    autoFocus: true,
    source: json,
    source: function(request, response) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response($.grep(json, function(item){return matcher.test(item.label); }) );
    },
    select: function( event, ui ) {
      $.ajax({ dataType:'script', type:'get', url: "/outsource_labs/" + outsource_lab_id+ "/outsource_lab_tests/new?lab_test_id=" + ui.item.id});
       $(this).val('') ;
      return false;
    } 

  });  
}


function report_outsource_lab_id_onchange() {
  $("#report_outsource_lab_id").select2().on("change", function(e) {
    if($(this).val() == ""){
      $('#report_outsource_lab_amount').val("0.0");
      $('.outsource_lab_amount').hide();
    }else{
      $('.outsource_lab_amount').show();
    }
  });
}





function confirm_patient_form() {

  $("form#new_patient").submit(function(e) {
    var patient_gender = $('#patient_gender').val();
    var patient_dob = $("#patient_dob").val();
    var patient_approximate_dob = $("#patient_approximate_dob").val();

    patient_gender_condition = (patient_gender == "" ? true : false);
    patient_dob_condition = ((patient_dob != "" || patient_approximate_dob != "")? false : true);


    if(patient_gender_condition && patient_dob_condition) {
      if(confirm('Are sure about creating patient without age and gender?')){
        return true;
      }
    }else if(patient_gender_condition && !patient_dob_condition) {
      if(confirm('Are sure about creating patient without gender?')){
        return true;  
      }
    }
    else if(!patient_gender_condition && patient_dob_condition) {
      if(confirm('Are sure about creating patient without age?')){
        return true;
      }
    }else{
      return true;
    }    

     return false; //is superfluous, but I put it here as a fallback
  });
}


function add_bill_to_receipt(current_link, patient_name, patient_no , venue_name, bill_no, billed_time, amount, bill_id){
  s = "<tr>";
  s += "<td> <input id='receipt_bill_ids_' class='bill-ids-select' type='checkbox' onclick='handleClick($(this),"+amount
    +");' value='"+ bill_id + "' name='receipt[bill_ids][]' > </td>";
  s += "<td>" + bill_no + "</td>";
  s += "<td>" + patient_name + "</td>";
  s += "<td>" + patient_no + "</td>";
  s += "<td>" + venue_name + "</td>";
  s += "<td>" + amount  + "</td>";
  s += "</tr>";
  reciepts_bill_arr.push(bill_id);
  //console.log( reciepts_bill_arr );
  $('#receipt_bills').append(s);
  bill_ids.push(bill_id);
  $('table#receipt_bills .bill-ids-select:last').prop("checked", true);
  $(current_link).hide();
  total_amount += parseFloat(amount);
  $('#total').html("<b>Total:</b>"+total_amount);
}

function check_bill_no_in_add_bills_already_present() {
  $("#receipt_bills_div").on("click", "a" ,function(event) { 

  $.ajax({
        data: 'bill_ids='+ reciepts_bill_arr, 
        dataType:'script', 
        type: 'get', 
        url: this.href
    });
  event.preventDefault();
     

  });
}

function get_bill_ids_on_click_add_bills() {
 

  $.ajax({
        data: 'bill_ids='+ reciepts_bill_arr, 
        dataType:'script', 
        type: 'get', 
        url: '/receipts/search_bills'
    });
 
}






function patient_selected_fn_in_bill_first_module (patient_id, name, patient_no, dob_blank, age, gender) {
  var c = "<table class='table table-bordered patient_details'>";
    c +=  "<tbody><tr><td> <b>Name :</b> </td> ";
    c +=  "<td>" + name + "</td>";
   
    c +=  "<td> <b>Patient no :</b> </td>"
    c +=  "<td>" + patient_no + "</td>";
    c +=  "<td> <b>";
    if(dob_blank == "true") {
      c += "Approximate age / Dob :"
    }else{
      c += "Dob / Age :" 
    }
    c +="</b> </td>"
    c +=  "<td>" + age  + "</td>";
    c +=  "<td> <b>Gender :</b> </td>";
    c +=  "<td>" + gender + "</td>";
  c +=  "</tr></tbody></table>";
  $("#patient_selected").html(c);
  $("#patient_id").val(patient_id)
  $('#registered_patient, #registered_mobile').modal('hide');
}

function handleClick(cb,amount){
  v = $(cb).val();
  if($(cb).is(':checked')){
    total_amount = total_amount + parseFloat(amount);
    reciepts_bill_arr.push(v)
  }
  else{
    total_amount = total_amount - parseFloat(amount);
    jQuery.each( reciepts_bill_arr, function( i, value ) {
      if(value == v){
        reciepts_bill_arr.splice(i, 1);
        return false;
      }
    });    
  }
  $('#total').html("<b>Total:</b>"+total_amount);
  //console.log(reciepts_bill_arr)
  $(cb).parents('tr').remove();
  
}





function billno_autocomplete() {
 
  $('#billno_autocomplete').autocomplete({ 
    minLength: 2,
    autoFocus: true,
    source: '/summaries/billno_autocomplete',
    select: function( event, ui ) {
      var s = "Bill Time ";
      s += ui.item.time;
       
      s += "   <a href='/summaries/update_opening_time?bill_id=" + ui.item.id  + "'  data-confirm='Are you sure?' class='btn btn-info'>Update Opening Time</a>"


       $("#change-text").html(s).show();

    } 
  });  
}


function confirm_add_to_bills() {
  html = modal_top_content ();

  html += "<h3> Do you want to add this result to bill? </h3>"
  html += "<a class='btn btn-info' onclick='$(\"input#add_to_bill_input\").val(\"yes\"); $(\"#form-report-save\").submit(); $(\"#add_to_bill\").modal(\"hide\"); return false;'   href='#'> Yes </a>";
  html += " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class='btn btn-danger' onclick='$(\"#form-report-save\").submit(); $(\"#add_to_bill\").modal(\"hide\"); return false;' href='#'> No </a>";

  html += modal_bottom_content();

  $('#add_to_bill').html(html);
  $('#add_to_bill').modal("show");
}

function modal_top_content () {
  return "<div class='modal-header'><button class='close' data-dismiss='modal' type='button'>×</button> <h3> &nbsp; </h3></div><div class='modal-body'>";
}

function modal_bottom_content() {
  return "</div><div class='modal-footer'><a href='#' data-dismiss='modal' class='btn'>Close</a></div>";
}

function remove_decimal_part(t) {
  s = parseInt(t);
  decimal = (t - s);
  if(decimal > 0.0){
    return t;
  }else{
    return s;
  }
}

function phone_no_enter_check_send_sms() {
  $("body").on("change", "input#patient_mobile" ,function() {
    var c = $(this).val();
    if(c.length >=1 ){
      $('#patient_send_sms').prop('checked', true);
    }else{
      $('#patient_send_sms').prop('checked', false);
    }
  });
}


function email_enter_check_send_email() {
  $("body").on("change", "input#patient_email", function() {
    var c = $(this).val();
    if(c.length >=1 ){
      $('#patient_send_email').prop('checked', true);
    }else{
      $('#patient_send_email').prop('checked', false);
    }
  });
}

function load_template(id) {
  if(id== ""){
   //$('.template').data("wysihtml5").editor.setValue('');
   $('.template').code('')
  }else{
  	$.getJSON( 'load_template', 
  	  { template_id: id },
  	  function( data ) {  
  	    //$('.template').data("wysihtml5").editor.setValue(data['content']);
  	    $('.template').code(data['content'])
  	}) 
  }  
}

function validateForRange(){
	$(".report_parameter").blur(function(event) { 
	  min_val = $(this).data("min") 
	  max_val = $(this).data("max") 
 
	  value =  $(this).val()
	  if (min_val != null) {
      if (parseFloat(value) < parseFloat(min_val/2) || parseFloat(value) > parseFloat(max_val*2) ){
        $('#' + this.id).addClass('out_of_range')
        //$('#' + this.id).parent().append("<span class='error_notice'> Recheck! </span>")
      }else{
      	$('#' + this.id).removeClass('out_of_range')
        $('#' + this.id).parent().children('.error_notice').remove();
      }
    }
  }) 
}

function prescription_type_on_change(prescription_type) {

  if (prescription_type == "IP"){

    $('.prescription_type_on_change_ip').show();
    $('.prescription_type_on_change_op').hide();
    show_ref_center();
   }
   else if (prescription_type == ""){
    $('.prescription_type_on_change_op').hide();
    $('.prescription_type_on_change_ip').hide();
    $('#prescription_referred_by_id').select2("val", "");
    change_lab_test_prices(center_id);
   }
   else{
    $('.prescription_type_on_change_op').show();
    $('.prescription_type_on_change_ip').hide();
    $('#prescription_referred_by_id').select2("val", "");
    change_lab_test_prices(center_id);
   }
}

function show_ref_center() {
  var value = $('#prescription_ip_no').val();
  console.log("sssssss"+value);
  if(value){
    //console.log("dddddddd "+ center_id)
    $('#prescription_referred_by_id').select2("val", center_id);
    change_lab_test_prices(center_id);
  }else{
    console.log("value is "+value);
  }
}

function result_option_sort() {
  $("#result_option_table_body").sortable({cursor: 'move',
    update: function(event, ui) {
      var order = $(this).sortable('toArray').toString();
      $.ajax({
        data: 'order='+ order, 
        dataType: 'script', 
        type: 'post', 
        url: '/lab_categories/'+lab_category_id+'/lab_tests/'+lab_test_id+'/result_option_sort'

      });
    }
  });
}

function receipt_search_form_submit_function() {
  $("body").on("submit", "form.receipt-search-form-submit",function(event) {
 
    $.ajax({
        data: $( this ).serialize() + '&bill_ids='+ reciepts_bill_arr , 
        dataType:'script', 
        type: 'get', 
        url: '/receipts/search_bills'
    });

    event.preventDefault();
    return false;
  });  
}

 
function qucik_print_report_ids(){
  var baseUrl = $(".quick_print_link").prop("href").split("?")[0];
  var ids = []
  $(".test_id:checked").each(function () {
     ids.push( $(this).attr("value") )
  
  });

  $(".quick_print_link").attr("href", baseUrl + '?report_ids=' + ids.join(',') )   
}

function get_email_from_center(current_value){
  var center_name = current_value;
  console.log(center_name);
  $.ajax({
        type: "GET",
        url: "/prescription_references/search_ref_center_email",
        cache: false,
        data: {term: current_value},
        dataType: "json",
        success : function (response) {
          console.log(response);
        $('#ref_re_email').val(response.email);
        
    }
  })
}

function get_venue_associates(current_value){
  var venue_id = ($(current_value)).val();
  console.log(venue_id);
  $.ajax({
        type: "GET",
        url: "/accountant/search_venues/search_venue_associates",
        cache: false,
        data: {term: venue_id},
        dataType: "script",
        success : function (response) {
          console.log(response);
        //$('#ref_re_email').val(response.email);
        
    }
  })
}
