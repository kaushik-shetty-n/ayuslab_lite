//= require jquery.editinplace.js

function printDiv(prescription_id) {
           /*w=window.open();
 
           w.document.write($('#labels').html());
           w.print(); 
           w.close();*/

           var input = $("<textarea>").attr("type", "hidden").attr("name", "html").val( $('#labels').html() );
 
           var newForm = $('<form>', {
            'action': prescription_id + "/print_strickers.pdf",
            'method': 'POST'
           }).append( $(input) );
           newForm.submit();

        }
        
$(document).ready(function() {
  /* Activating Best In Place */
  $(".best_in_place").editInPlace({
		callback: function(original_element, html, original){
			return(html);
		}
	});
});

function cloneSticker(that){
 html = "<div class='sticker_wrapper'>" + $(that).parent().html() + "</div>" 
 $(that).parent().after(html );
}
