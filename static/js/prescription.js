jQuery(document).ready(function() {
  $("body").on("change", "select#prescription_referred_by_id",function() {
     change_lab_test_prices($(this).val());
     check_print_ref_center($(this).val());
  });
  $("body").on("change", "select#prescription_doctor_id",function() {
    changed_doctor($(this).val());
  });
 
});

function changed_doctor(doctor_id) {
  if(doctor_id == ""){
    $('#doctor-qualification').html('');
  }else{
    $.each(doctors_json, function(index) {
      if(doctors_json[index].id == doctor_id){
        $('#doctor-qualification').html(doctors_json[index].qualification);
        return false;
      }
    });

  }
}

function labtestautocomplete(lab_tests) {
  var json = lab_tests;
  $('#lab_testname').autocomplete({ 
    minLength: 2,
    autoFocus: true,
    source: json,
    source: function(request, response) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response($.grep(json, function(item){return matcher.test(item.label); }) );
    },
    select: function( event, ui ) {
      if(ui.item.type == "LabTest"){
        $("input[type='checkbox'][class='test-test-id-"+ui.item.id+"']:first").prop("checked", true);
        var pr = find_price_of_lab_test(ui.item.id, ui.item.price);
        prescription_bill_items.push({"lab_test_id" : ui.item.id, "type" : "LabTest", "price" : pr}); // add to array
        add_bill_item_to_bill("LabTest", ui.item.label, ui.item.id,  pr); // function to add rows to bills
        add_lab_test_to_prescribed_tests( ui.item.label, ui.item.id, ui.item.specimen_master_id); 
        if(ui.item.specimen_master_id != "") {
          add_to_samples(ui.item.label, ui.item.id, ui.item.specimen_master_id);
        }  
      }else{
        $("input[type='checkbox'][class='bill-item-master-id-"+ui.item.id+"']:first").prop("checked", true);
        prescription_bill_items.push({"bill_item_master_id" : ui.item.id , "type" : "BillItemMaster", "price" : ui.item.price});
        add_bill_item_to_bill("BillItemMaster", ui.item.label, ui.item.id, ui.item.price )
      }
      calcaulate_total_price();
      var d = "<div class='alert alert-success' style='height: 15px'>";
      d += ui.item.label ;
      d += " selected </div>";
      add_lab_test_prescribed_lists(ui.item.label, ui.item.id, ui.item.type);
      $('#alert-area').html(d).show().delay(1000).fadeOut("slow", function () { $(this).hide(); });      
      $(this).val('');
      $('.add-sample-links').show();
      return false;

    } 
  });  
}

function labtestautocomplete_for_doctor_incentive(lab_tests) {
  var json = lab_tests;
  $('#lab_testname').autocomplete({ 
    minLength: 2,
    autoFocus: true,
    source: json,
    source: function(request, response) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response($.grep(json, function(item){return matcher.test(item.label); }) );
    },
    select: function( event, ui ) {
      console.log(ui.item.type)
      console.log(ui.item.id)
      if(ui.item.type == "LabTest"){
       var c = 'lab-test-id-' + ui.item.id;
       console.log("c")
       add_lab_test_to_prescribed_tests( ui.item.label, ui.item.id, ui.item.specimen_master_id);
       enable_or_disable_autocomplete_options();


        $(".lab_test").each(function () { 
          console.log("ddddddddd")
          if ($(this).hasClass(c)){
            console.log(c)
            $(this).prop("checked", true);
            return false;
          }
        })  
      }else{
        $("input[type='checkbox'][class='bill-item-master-id-"+ui.item.id+"']:first").prop("checked", true);
      }
      return false;

    } 
  });  
}


function enable_or_disable_autocomplete_options() { // disble lab test once added to prescribed test
  var lab_test_ids_prescribed = [];
  $.each(prescribed_tests, function(index) {
    lab_test_ids_prescribed.push(prescribed_tests[index].lab_test_id)
  });

  autocomplete_array = jQuery.grep(venue_bill_items, function(element) {
    return jQuery.inArray(element.id, lab_test_ids_prescribed ) == -1;
  });
  $('#lab_testname').autocomplete('option', 'source', autocomplete_array);  
}


function change_lab_test_prices(reference_center_id) { // 
  $.each(prescription_bill_items, function(index) {
    if(prescription_bill_items[index].type == "LabTest"){
      ld = prescription_bill_items[index].lab_test_id ;
      p = find_price_of_lab_test(ld, lab_test_price(ld));
      prescription_bill_items[index].price = p;
      $("table#bill_items .lab_test_" +ld+":last").parents("tr").find(".bill_amount:last").val(p);
      total_calculation();
    }
  });
}

function lab_test_price(lab_test_id) { // find lab test price
  var lp = 0.0;
  $.each(lab_test_json, function(index) {
    if(lab_test_json[index].lab_test_id == lab_test_id){
      lp = lab_test_json[index].price;
      return false;
    }
  });    
  return lp;
}


function find_price_of_lab_test(lab_test_id, price) { // if ref center selected then return ref center lab test amount otherwise lab test price
  var reference_center_id = $('#prescription_referred_by_id').val();
  if(reference_center_id != ""){
    var ref_price = price;
    $.each(ref_center_price_lists, function(index) {
      if((ref_center_price_lists[index].prescription_reference_id == reference_center_id) && (ref_center_price_lists[index].lab_test_id == lab_test_id)){
        ref_price = ref_center_price_lists[index].price;
        return false;
      }
    });
    return ref_price;
  }else{
    return price;
  }
}


function add_lab_test_prescribed_lists(test_name, id, t) {
  if(t == "LabTest"){
    d = "label-info lab-test-added-" + id;
  }else{
    d = "label-bill-item-master bill-item-added-" + id;
  }
  var s = "<span class='label " + d +"'> " + test_name ;
  if(t == "LabTest"){
    s += "<a class='danger-color' onclick=\"remove_lab_test_from_prescription('"+id+"'); return false;\" href='#'> x</a>";
  }else{
    s += "<a class='danger-color' onclick=\"remove_bill_master_from_prescription('"+id+"'); return false;\" href='#'> x</a>";
  }
  s += "</span>";
  $('#lab-test-prescribed-lists').append(s);
}




function add_package_to_bill_items(package_id) {
  var package_lab_tests;
  var package_price;
  var packagename = "";
  $.each(packages_json, function(index) {
    if(packages_json[index].id == package_id){
      package_lab_tests = packages_json[index].lab_test_ids;
      package_price = packages_json[index].price;
      packagename = packages_json[index].name;
      return false;
    }
  }); 
  prescription_bill_items.push({"package_id" : package_id , "type" : "Package", "price" : package_price, "lab_tests_ids" : package_lab_tests})
  add_bill_item_to_bill("Package", packagename, package_id, package_price)
  calcaulate_total_price();
}

function remove_package_to_bill_items(package_id) {
  jQuery.each( prescription_bill_items, function( i, array ) {
    if(array["type"] == "Package" && array["package_id"] == package_id){
      prescription_bill_items.splice(i, 1);
      return false;
    }
  });  
  remove_bill_item_from_bill("Package",  package_id);
  calcaulate_total_price();
}



function calcaulate_total_price() {
  total_price = 0.0;
  var all_lab_ids = [];
  jQuery.each( prescription_bill_items, function( i, array ) {
    if(array["type"] == "Package"){
      all_lab_ids = all_lab_ids.concat(array["lab_tests_ids"]);
    }
  });  

  jQuery.each( prescription_bill_items, function( i, array ) {
    if(array["type"] == "Package"){
      total_price += parseFloat(array["price"]);
    }else if(array["type"] == "LabTest"){
      if(jQuery.inArray(array["lab_test_id"], all_lab_ids ) == -1) {
        total_price += parseFloat(array["price"]);
      }
    }else{
      total_price += parseFloat(array["price"]);
    }
  });  

  $('span#total_price').html(total_price);
  total_calculation();
}

function add_or_remove_lab_tests(ch, lab_test_name, lab_test_id, t, price, specimen_master_id) {
  var checked = $(ch).prop('checked');
  if(checked){

    p = find_price_of_lab_test(lab_test_id, lab_test_price(lab_test_id));
    add_lab_test_prescribed_lists(lab_test_name, lab_test_id, t);
    prescription_bill_items.push({"lab_test_id" : lab_test_id , "type" : "LabTest", "price" : p});
    add_bill_item_to_bill("LabTest", lab_test_name, lab_test_id, p );
    add_lab_test_to_prescribed_tests(lab_test_name, lab_test_id, specimen_master_id);
    if(specimen_master_id != "") {
      add_to_samples(lab_test_name, lab_test_id, specimen_master_id);
    }     
  }else{
    $('.lab-test-added-'+ lab_test_id).remove();
    jQuery.each( prescription_bill_items, function( i, array ) {
      if(array["type"] == "LabTest" && array["lab_test_id"] == lab_test_id){
        prescription_bill_items.splice(i, 1);
        return false;
      }
    });  
    remove_lab_test_to_prescribed_tests(lab_test_id);
    remove_bill_item_from_bill("LabTest",  lab_test_id);
  }  
  calcaulate_total_price(); 
}



function add_or_remove_bill_item_masters(ch, bill_item_master_name, bill_item_master_id, t, price) {
  var checked = $(ch).prop('checked');
  if(checked){
    add_lab_test_prescribed_lists(bill_item_master_name, bill_item_master_id, t);
    prescription_bill_items.push({"bill_item_master_id" : bill_item_master_id , "type" : "BillItemMaster", "price" : price});
    add_bill_item_to_bill("BillItemMaster", bill_item_master_name, bill_item_master_id, price );
  }else{
    $('.bill-item-added-'+ bill_item_master_id).remove();
    jQuery.each( prescription_bill_items, function( i, array ) {
      if(array["type"] == "BillItemMaster" && array["bill_item_master_id"] == bill_item_master_id){
        prescription_bill_items.splice(i, 1);
        return false;
      }
    });
    remove_bill_item_from_bill("BillItemMaster",  bill_item_master_id);    
  }  
  calcaulate_total_price(); 
}


function package_selected(package_id) {
  $.each(packages_json, function(index) {
    if(packages_json[index].id == package_id){
      var c = packages_json[index].lab_test_ids;
      $.each(c, function(i) { 
        $('input[value="'+c[i]+'"]').prop("checked", true);
        $.each(lab_test_json, function(index, mt) {
          if(c[i] == mt.lab_test_id) {
            add_lab_test_prescribed_lists(mt.label, mt.lab_test_id, "LabTest");
            add_lab_test_to_prescribed_tests(mt.label, mt.lab_test_id,mt.specimen_master_id)
            return false;
          }
        });
      });
      return false;
    }
  });
}


function un_package_selected(package_id) {
  $.each(packages_json, function(index) {
    if(packages_json[index].id == package_id){
      var c = packages_json[index].lab_test_ids;
      $.each(c, function(i) { 
        $('input[value="'+c[i]+'"]').prop("checked", false);
        $('.lab-test-added-'+c[i]).remove();
        remove_lab_test_to_prescribed_tests(c[i]);
      });
      return false;
    }
  });
}


function remove_lab_test_from_prescription(lab_test_id) {
  $('span.lab-test-added-'+ lab_test_id).remove();
  $("input[type='checkbox'][class='test-test-id-"+lab_test_id+"']:first").prop("checked", false);

  jQuery.each( prescription_bill_items, function( i, array ) {
    if(array["type"] == "LabTest" && array["lab_test_id"] == lab_test_id){
      prescription_bill_items.splice(i, 1);
      return false;
    }
  });  
  remove_bill_item_from_bill("LabTest", lab_test_id);
  remove_lab_test_to_prescribed_tests(lab_test_id);
  calcaulate_total_price();   
}

function remove_bill_master_from_prescription(bill_item_master_id) {
  $('span.bill-item-added-'+ bill_item_master_id).remove();
  $("input[type='checkbox'][class='bill-item-master-id-"+bill_item_master_id+"']:first").prop("checked", false);
  jQuery.each( prescription_bill_items, function( i, array ) {
    if(array["type"] == "BillItemMaster" && array["bill_item_master_id"] == bill_item_master_id){
      prescription_bill_items.splice(i, 1);
      return false;
    }
  });
  remove_bill_item_from_bill("BillItemMaster",  bill_item_master_id);  
  calcaulate_total_price();
}



function add_sample_to_prescription(){
  var new_id = new Date().getTime(); 
  string = "<tr>"
    string += "<td> " + generate_specimen_master_select(new_id, 0) + "</td>"
    string += "<td> "
    string += "<input type='text' value='' name='sample["+new_id+"][sample_no]' id='sample_"+ new_id +"_sample_no' class='string optional input-medium'>" 
    string += "</td>"
    string += "<td> "

    string += "<a href='#' onclick=\"$('#specimen_master_"+new_id +"').toggle(); return false;\"><i>lab tests </i></a>";
    string += "<div class='hide' id='specimen_master_"+ new_id + "'>";
   
        
    jQuery.each( prescribed_tests, function( i, array ) {
      string += add_lab_tests_to_specimen(array["name"], array["lab_test_id"], new_id); 
    });

    string += "</div>"  
    string += "</td>"
    string += "<td>"
      string += "<a href='#' class='btn btn-mini btn-danger' onclick=\"$(this).parents('tr').remove(); return false;\">x</a>";
    string += "</td>"
  string += "</tr>"

  $('table#prescription_sample_details').append(string); 

}


function add_lab_test_to_prescribed_tests(name, lab_test_id, specimen_master_id) {
  prescribed_tests.push({"lab_test_id" : lab_test_id , "name" : name, "specimen_master_id": specimen_master_id});
  show_add_sample_link();
  enable_or_disable_autocomplete_options();
  console.log(prescribed_tests);
}

function remove_lab_test_to_prescribed_tests(lab_test_id) {
  jQuery.each( prescribed_tests, function( i, array ) {
    if(array["lab_test_id"] == lab_test_id){
      prescribed_tests.splice(i, 1);
      return false;
    }
  });
  show_add_sample_link();
  $("label.lab_test_"+lab_test_id).remove();
  enable_or_disable_autocomplete_options();

}


function show_add_sample_link() {
  if(prescribed_tests.length >= 1){
    $('.add-sample-links').show();
  }else{
    $('.add-sample-links').hide();
    var rowcount = $("table#prescription_sample_details tr").length;
    while (rowcount > 1) {
      $("table#prescription_sample_details tr:last-child").remove();
      rowcount -= rowcount;
    }      
  }
}

function add_to_samples(lab_test_name, lab_test_id, specimen_master_id) {
  var new_id = new Date().getTime(); 

  var specimen_master_present = $("table#prescription_sample_details tr.specimen_master_"+specimen_master_id+":first").attr('class');
  string = "";
  if (typeof specimen_master_present != 'undefined'){
    previous_id_string = $("table#prescription_sample_details tr.specimen_master_"+specimen_master_id+":first").find('select').first().attr('id');
    new_id = previous_id_string.split("sample_")[1].split("_specimen_master_id")[0];
    string += add_lab_tests_to_specimen(lab_test_name, lab_test_id, new_id);
    $("#specimen_master_"+ specimen_master_id).append(string);
  }else{
    string = "<tr class='specimen_master_" +specimen_master_id+ "'>"
      string += "<td>" + generate_specimen_master_select(new_id, specimen_master_id) + " </td>"
      string += "<td>"
      string += "<input type='text' value='' name='sample["+new_id+"][sample_no]' id='sample_"+ new_id +"_sample_no' class='string optional input-medium'>"
      string += "</td>"
      string += "<td>";
      string += "<a href='#' onclick=\"$('#specimen_master_"+specimen_master_id +"').toggle(); return false;\"><i>lab tests </i></a>";
      string += "<div class='hide' id='specimen_master_"+ specimen_master_id + "'>";
      string += add_lab_tests_to_specimen(lab_test_name, lab_test_id, new_id);
      string += "</div>"

       
      string += "</td>";
      string += "<td>"
        string += "<a href='#' class='btn btn-mini btn-danger' onclick=\"$(this).parents('tr').remove(); return false;\">x</a>";
      string += "</td>"
    string += "</tr>"
    $('table#prescription_sample_details').append(string); 
  }  
}

function generate_specimen_master_select(new_id, specimen_master_id) {
  var s = "<select name='sample["+new_id+"][specimen_master_id]' id='sample_"+ new_id +"_specimen_master_id'>";
  jQuery.each( specimen_masters_json, function( i, array ) {
    if(array["id"] == specimen_master_id) {
      s += "<option selected='selected' value='"+array["id"]+ "'>" + array["specimen"] + "</option>";  
    }else{ 
      s += "<option value='"+array["id"]+ "'>" + array["specimen"] + "</option>";
    }
  });
  s += "</select>";
  return s ;   
}

function add_lab_tests_to_specimen(lab_test_name, lab_test_id, new_id) {
  t = "<label class='checkbox lab_test_"+lab_test_id+"'>"
  t += "<input class='sample_specimen_lab_test_checkbox' id='sample_"+lab_test_id+"_lab_test_ids_' type='checkbox' value='"+lab_test_id+"' name='sample["+new_id+"][lab_test_ids][]' checked='checked' onclick='sample_lab_tests_checkbox_click($(this));' >"
  t += lab_test_name;
  t += "</label>";
  return t;
}

function sample_lab_tests_checkbox_click(clicked) {
  var checked = $(clicked).prop('checked');
  if(!checked){
    var c = $(clicked).parents('tr').find("input.sample_specimen_lab_test_checkbox:checked").length;
    if(c == 0) {
      $(clicked).parents('tr').remove();
    }
  }   
}


function sample_lab_tests_checkbox_click_edit(clicked) {
  var checked = $(clicked).prop('checked');
  if(!checked){
    var c = $(clicked).parents('tr').find("input.sample_specimen_lab_test_checkbox:checked").length;
    if(c == 0) {
      $(clicked).parents("tr").find('.destroy_sample:first').val(1);
      $(clicked).parents("tr").removeClass().addClass('hide');
    }
  }   
}



function add_package_lab_tests_to_specimen(package_id) {
  $.each(packages_json, function(index) {
    if(packages_json[index].id == package_id){
      var c = packages_json[index].lab_test_ids;
      $.each(c, function(i) { 
        $('input[value="'+c[i]+'"]').prop("checked", true);
        $.each(lab_test_json, function(index, mt) {
          if((c[i] == mt.lab_test_id) && mt.specimen_master_id != "") {
            add_to_samples(mt.label, mt.lab_test_id, mt.specimen_master_id);
            return false;
          }
        });

      });
      return false;
    }
  }); 
}

function confirm_prescription_form() {
  $("form#bill-first-module-form").submit(function(e) {
    var patient_name = $('#patient_name').val();
    
    if(patient_name == ""){
      alert('Please enter patient name');
      return false;
    }

    if(prescription_bill_items.length == 0) {
      alert('Please add atleast one lab tests');
      return false;
    }
    


    var patient_gender = $('#patient_gender').val();
    var patient_dob = $("#patient_dob").val();
    var patient_approximate_dob = $("#patient_approximate_dob").val();
    var referred_by_doctor = $("#prescription_doctor_id").val();

    patient_gender_condition = (patient_gender == "" ? true : false);
    patient_dob_condition = ((patient_dob != "" || patient_approximate_dob != "")? false : true);
    referred_by_doctor_condition = (referred_by_doctor == "" ? true : false);
    s = "Are sure about creating patient without "

    if(patient_gender_condition) {
      s += "gender,"
    }
    if(patient_dob_condition){
      s += "age,"
    }
        
    if(referred_by_doctor_condition) {
      s += "referred by"
    }
    s += " ?"

    if(patient_gender_condition || patient_dob_condition || referred_by_doctor_condition) {
      if(confirm(s)){
        lab_test_as_asc();
        return true;
      }
    }else{
      lab_test_as_asc();
      return true;
    }    
 
    return false; //is superfluous, but I put it here as a fallback
  });
}


function remove_sample_details_object(current) {
  $(current).parents("tr").find('.destroy_sample:first').val(1);

  $(current).parents("tr").removeClass().addClass('hide');

}

function check_print_ref_center(v) {
 
    if (v !== ""){
      $('#prescription_print_referred_by').prop("checked", true);
    }
    else{
      $('#prescription_print_referred_by').prop("checked", false);
    }

  
}


function lab_test_as_asc() {
  var lab_array = [];
  $.each(prescribed_tests, function(index) {
    lab_array.push(prescribed_tests[index].lab_test_id)
  });

  $.each(lab_array, function( index, value ) {
    $("input[type='checkbox'][class='test-test-id-"+value+"']:first").prop("checked", false);
  });

  $("#lab_tests_string").val(lab_array.join());
}