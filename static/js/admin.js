// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require jquery-ui-1.10.3.custom
//= require wysihtml5-0.3.0.js
//= require bootstrap-wysihtml5
//= require bootstrap-modalmanager.js
//= require bootstrap-modal.js
//= require jquery.remotipart
//= require summernote.min
//= require common

jQuery(document).ready(function() {
  test_parameter_sort();
  $(".datepicker").datepicker({dateFormat: 'dd-mm-yy',changeMonth: true, changeYear: true });
});


function jquery_text_editor () {
  //$('.html-editor').wysihtml5();
  $('.html-editor').summernote({
    width: "595px"
  });
  
  $('.note-help').remove();
  //$('.html-editor').redactor({ fixed: true })  
}  

function test_parameter_sort() {
  $("#lab_test_settings").sortable({cursor: 'move',
    update: function(event, ui) {
      var order = $(this).sortable('toArray').toString();
      $.ajax({
        data: 'order='+ order, 
        dataType: 'script', 
        type: 'get', 
        url: '/admin/master_lab_categories/'+lab_category_id+'/master_lab_tests/'+lab_test_id+'/sort'
      });
    }
  });
}

function master_result_option_sort() {
  
  $("#master_result_option_table_body").sortable({cursor: 'move',
    update: function(event, ui) {
      var order = $(this).sortable('toArray').toString();
      $.ajax({
        data: 'order='+ order, 
        dataType: 'script', 
        type: 'post', 
        url: '/admin/master_lab_categories/'+lab_category_id+'/master_lab_tests/'+lab_test_id+'/master_result_option_sort'

      });
    }
  });
}
