function add_fields(appendid, association, content) {  
  var new_id = new Date().getTime();  
  var regexp = new RegExp("new_" + association, "g");  
  if(appendid == "bill_items") {
    var tr_count = $("table#bill_items tr").length;
    $('table#bill_items tr').eq(tr_count - 3).after(content.replace(regexp, new_id));
  }else {
    $('#'+appendid).append(content.replace(regexp, new_id));
  }
} 


function delete_payment_details(current_link) {
  var row = $(current_link).parents('tr');
  row.hide();
 
}

jQuery(document).ready(function() {
 
  $(document).ajaxStart(function(){
    $("#progress").slideDown();
  });
   
  $(document).ajaxStart(function(){
    $("#progress").slideUp();
  }); 
});   