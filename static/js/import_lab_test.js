function do_you_want_to_add_all_lab_tests_from_other_venue(check_box_clicked) {
  var checked = $(check_box_clicked).prop('checked');
  if(checked){
    var confirmed = confirm("Do you want to add all lab tests in this category?");
    if (confirmed==true){
      var v = $(check_box_clicked).val();
      var css_class = "lab_category_" + v;
      $("input:checkbox[class='"+css_class+"']").prop("checked", true);
    }
  }else{
    var css_class = "lab_category_" + $(check_box_clicked).val();
    $("input:checkbox[class='"+css_class+"']").prop("checked", false);
  }
  $('#import-lab-test-div').show();
}



function check_for_lab_category_ticked(lab_check_box_clicked) {
  var c = $(lab_check_box_clicked).attr('class').split('lab_category_')[1];
  var lab_category = $(".lab-category-" + c + ":first");
  var f = lab_category.attr('disabled')
  var checked = lab_category.prop('checked');
  if(typeof f == 'undefined'){
    var disabled = false;
  }else{
    var disabled = true;
  }
  
  if(!(checked || disabled)) {
    var  message = 'selected corresponding category ';
    alert(message);
    $(".lab-category-" + c + ":first").attr('checked', true);
  }
  $('#import-lab-test-div').show();
}
