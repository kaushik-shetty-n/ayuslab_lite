jQuery(document).ready(function() {
  referred_by_id_eq_select_fn();
});


function referred_by_id_eq_select_fn() {
  $('.referred_by_id_eq_select').multiselect(
    {
      enableFiltering: true, nonSelectedText: "Select Ref. Center",
      enableCaseInsensitiveFiltering: true,
      onChange:function(element, checked){
        if(checked) { 
          s = "<div class='label label-ref ref_center_" + element.val() + "'>";
          s += element.text();
          s += "</div>"

          $('#ref_centers').append(s)
        }else { 
        	$('.ref_center_'+element.val()).remove();
        }
      }, 
      buttonText: function(options, select) {
        if (options.length == 0) {
          return 'Select Ref. Center <b class="caret"></b>';
        }
        else if (options.length > 2) {
            return options.length + ' Ref. Centers selected <b class="caret"></b>';
        }
        else {
            var selected = '';
            options.each(function() {
                selected += $(this).text() + ', ';
            });
            return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
        }
    }
  });
}