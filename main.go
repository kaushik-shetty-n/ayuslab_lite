package main


import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	//models "ayuslab_lite/models"
	_ "ayuslab_lite/routers"
    _ "github.com/go-sql-driver/mysql"
    "html/template"
    //"fmt"
)

func male(gender string)(b bool){
  if gender == "male"{
    b = true
  }
  return b
}

func pop_up(str string) (s template.HTML){
  //a_tag := ""
  //a_tag = a_tag + `<a class="label label-info" data-content=`+ str 
  //a_tag = a_tag +`data-original-title="<b> Lab Tests </b>" data-placement="left" data-trigger="hover" rel="popover">`
  //fmt.Printf("Result Nums: %s\n", str)
  //a_tag = a_tag + "Lab Tests"
  //a_tag = a_tag +`</a>`
  s = template.HTML(str)
  return s
}


func init() {
  orm.RegisterDriver("mysql", orm.DRMySQL)
  orm.RegisterDataBase("default", "mysql", "root:isiri@/ayuslab_development?charset=utf8")
  beego.AddFuncMap("male",male)
  beego.AddFuncMap("pop_up",pop_up)
}

func main() {
	orm.Debug = true
	beego.Run()
}
