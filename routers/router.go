package routers

import (
	"ayuslab_lite/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})
    beego.Router("/dashboard", &controllers.WorkspaceController{}, "get:Dashboard")
    beego.Router("/patients", &controllers.PatientsController{}, "get:Index")
    beego.Router("/prescriptions", &controllers.PrescriptionsController{}, "get:Index")
    beego.Router("/reports", &controllers.ReportsController{}, "get:Index")
    beego.Router("/bills", &controllers.BillsController{}, "get:Index")
}
